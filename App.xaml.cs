﻿using System;
using System.Windows;

using VectorAneurysm.Views;

namespace VectorAneurysm
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Handles app startup. If there is one agrument, it's treated as file path.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Event arguments, including cmd arguments, if any.</param>
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            MainWindow w = new MainWindow();
            if (e.Args.Length == 1)
            {
                try
                {
                    w.OpenFile(e.Args[0]);
                }
                catch (ApplicationException)
                {
                    Current.Shutdown();
                }
            }
            w.Show();
        }
    }
}
