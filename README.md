# Векторная аневризма
#### (унылая) векторная рисовалка для «технологий разработки программного обеспечения»

![ремастер искусства в почти финальной версии программы](https://i.imgur.com/WLrgIvK.png)  
[(шедевр в родном формате без регистрации и смс)](https://cloud.bnfour.net/s/7DBN7FBoM3F9T5y/download)  
.NET 4.7, WPF, MVVM, VS 2017, все дела  

### Хитрый план релизов с дидлайнами
- [x] Лаба 1, релиз 0.3 — рисование фигурок, **релиз 12/11/18**  
- [x] Лаба 2, релиз 0.6 — редактирование нарисованных фигурок, **релиз 27/11/18**  
- [x] Лаба 3, релиз 1.0 — прочий обвес приличной программы, **релиз 6/12/18**

Ну такое себе получилось, да.