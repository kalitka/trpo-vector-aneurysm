﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Input;

using VectorAneurysm.DataStructures;
using VectorAneurysm.Models.Operations;
using VectorAneurysm.Models;
using VectorAneurysm.Util;
using VectorAneurysm.Views;

namespace VectorAneurysm.ViewModels
{
    /// <summary>
    /// View model for main window. Exposes stuff for binding to UI,
    /// also routes events from UI to relevant calls.
    /// Is this what viewmodel means?
    /// </summary>
    public partial class DrawingViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Shape that is used to store currently selected stroke
        /// </summary>
        public Shape Palette { get; } = new Rectangle();

        /// <summary>
        /// Holds canvas data.
        /// </summary>
        private readonly CanvasModel _canvas = new CanvasModel();

        /// <summary>
        /// Backing field for <see cref="CurrentTool"/> property;
        /// </summary>
        private SelectedTool _tool = SelectedTool.Selection;

        /// <summary>
        /// Holds the currently selected tool.
        /// </summary>
        public SelectedTool CurrentTool
        {
            get => _tool;
            set
            {
                if (value != _tool)
                {
                    _tool = value;
                    NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Width of used canvas.
        /// </summary>
        public double CanvasWidth
        {
            get => _canvas.Width;
            set
            {
                if (value != _canvas.Width)
                {
                    _canvas.Width = (int)value;
                    NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Height of used canvas.
        /// </summary>
        public double CanvasHeight
        {
            get => _canvas.Height;
            set
            {
                if (value != _canvas.Height)
                {
                    _canvas.Height = (int)value;
                    NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Backing field for <see cref="FilePath"/> property.
        /// </summary>
        private string _filePath;

        /// <summary>
        /// Path to currently opened file.
        /// </summary>
        private string FilePath
        {
            get => _filePath;
            set
            {
                if (value == null || !value.Equals(_filePath))
                {
                    _filePath = value;
                    NotifyPropertyChanged(nameof(WindowTitle));
                }
            }
        }

        /// <summary>
        /// Backing field for <see cref="IsChanged"/> property.
        /// </summary>
        private bool _isChanged;

        /// <summary>
        /// Indicates whether currently opened file has unsaved changes.
        /// </summary>
        private bool IsChanged
        {
            get => _isChanged;
            set
            {
                if (value != _isChanged)
                {
                    _isChanged = value;
                    // pepeHands
                    NotifyPropertyChanged(nameof(WindowTitle));
                }
            }
        }

        /// <summary>
        /// Property to hold window title to display.
        /// </summary>
        public string WindowTitle
        {
            get
            {
                var unsavedMark = _isChanged ? "*" : "";
                var expandedPath = _filePath ?? "Untitled";
                return $"{unsavedMark}{expandedPath} — Vector Aneurysm";
            }
        }

        /// <summary>
        /// Both canvas' dimensions as a tuple.
        /// </summary>
        public (double, double) CanvasSize => (CanvasWidth, CanvasHeight);

        /// <summary>
        /// List of available stroke types.
        /// </summary>
        public IEnumerable<StrokeTypeItem> StrokeTypes { get; }
            = StrokeTypesFactory.GetStrokeTypes();

        // Canvas.Children cannot be bound in xaml easily, so here it is
        // to be "bound" in code behind
        /// <summary>
        /// Holds everything that should be drawn to canvas. Currently it's already
        /// drawn figures, the one being drawn right now. TODO: selction resize handles.
        /// </summary>
        public ObservableCollection<Shape> CanvasShapes { get; }
            = new ObservableCollection<Shape>();

        /// <summary>
        /// <see cref="ShapeDrawer"/> instance to use within this model.
        /// </summary>
        private readonly ShapeDrawer _drawer;

        /// <summary>
        /// Instance of <see cref="SelectionManager"/> to use within this model.
        /// </summary>
        private readonly SelectionManager _selection;

        /// <summary>
        /// Instance of <see cref="OperationManager"/> to use within this model.
        /// </summary>
        private readonly OperationManager _undoer;

        /// <summary>
        /// Class constructor.
        /// </summary>
        public DrawingViewModel()
        {
            _drawer = new ShapeDrawer(this);
            _selection = new SelectionManager(this);
            _undoer = new OperationManager(_selection.Cleanup);

            Palette.Fill = new SolidColorBrush(Color.FromRgb(202, 80, 32));
            Palette.Stroke = new SolidColorBrush(Color.FromRgb(96, 37, 7));
            Palette.StrokeThickness = 1;
            Palette.StrokeDashArray = null;

            _canvas.Figures.CollectionChanged += UpdateCanvasShapes;
            _canvas.Figures.CollectionChanged += _selection.OnCanvasFiguresChanged;
            _selection.Markers.CollectionChanged += UpdateCanvasShapes;

            _drawer.DrawCancelled += OnDrawCancelled;
            _drawer.DrawCompleted += OnDrawCompleted;
            _drawer.DrawStarted += OnDrawStarted;

            _selection.SelectionDrawer.DrawStarted += OnDrawStarted;
            _selection.SelectionDrawer.DrawCancelled += OnDrawCancelled;
            _selection.SelectionDrawer.DrawCompleted += OnSelectionDrawCompleted;

            _undoer.OnOperartionAdded += (s, e) =>
            {
                IsChanged = true;
                // see OperationManager.EmitOperationAdded() for explanations
                if (!e.Equals(EventArgs.Empty))
                {
                    NotifyPropertyChanged(nameof(CanvasWidth));
                    NotifyPropertyChanged(nameof(CanvasHeight));
                }
            };
        }

        /// <summary>
        /// Resets everything to blank state.
        /// </summary>
        private void CreateNew()
        {
            _canvas.Figures.RemoveByOne();
            _canvas.AllFigures.Clear();
            _undoer.Clear();
            FilePath = null;
            IsChanged = false;
            CanvasWidth = CanvasModel.DefaultWidth;
            CanvasHeight = CanvasModel.DefaultHeight;
        }

        /// <summary>
        /// Raised when a public setable property (e.g <see cref="CurrentTool"/>) on
        /// paste command is changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Fires <see cref="PropertyChanged"/> event.
        /// </summary>
        /// <param name="propertyName">Name of the property that changed.
        /// Filled in automagically.</param>
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Handles Edit->Canvas->Resize click.
        /// </summary>
        public void HandleCanvasResize()
        {
            var resizerWindow = new CanvasResizeWindow((int)CanvasWidth,
                (int)CanvasHeight);
            var dialogResult = resizerWindow.ShowDialog();
            if (dialogResult.IsTrue())
            {
                var newSize = resizerWindow.NewSize;
                var dx = newSize.Item1 - CanvasWidth;
                var dy = newSize.Item2 - CanvasHeight;
                var p = new Point(dx, dy);
                _undoer.AddOperation(new CanvasResizeOperation(_canvas, p));
                _selection.Cleanup();
            }
        }

        /// <summary>
        /// Event handler for ObservableCollections changes, syncs up content
        /// to one collection bound to canvas to display everything.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Event arguments.</param>
        private void UpdateCanvasShapes(object sender,
            NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (Shape figure in e.NewItems)
                    {
                        CanvasShapes.Add(figure);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (Shape figure in e.OldItems)
                    {
                        CanvasShapes.Remove(figure);
                    }
                    break;
            }
        }

        /// <summary>
        /// Reroutes left click events to appropriate helper class.
        /// </summary>
        /// <param name="pos">Mouse cursor position relative to Canvas.</param>
        /// <param name="hitShape">Shape that was hit by click, null if none.</param>
        public void HandleLeftClick(Point pos, Shape hitShape)
        {
            if (CurrentTool == SelectedTool.Selection)
            {
                _selection.HandleLeftClick(hitShape);
            }
            else
            {
                _drawer.HandlePrimaryDraw(pos, CurrentTool);
            }
        }

        /// <summary>
        /// Reroutes left mouse down event to preform marker drap'n'drop.
        /// </summary>
        /// <param name="pos">Mouse cursor position relative to Canvas.</param>
        /// <param name="hitShape">Shape that was hit by click, null if none.</param>
        public void HandleLeftDown(Point pos, Shape hitShape)
        {
            if (CurrentTool == SelectedTool.Selection)
            {
                _selection.HandleLeftMouseDown(pos, hitShape);
            }
        }

        /// <summary>
        /// Reroutes right mouse down event to start drawing selection recatangle if
        /// current tool is indeed selection.
        /// </summary>
        /// <param name="pos">Mouse cursor position relative to Canvas.</param>
        public void HandleRightDown(Point pos)
        {
            if (CurrentTool == SelectedTool.Selection)
            {
                _selection.HandleRightClickDown(pos);
            }
        }

        /// <summary>
        /// Reroutes mouse move events to appropriate helper class.
        /// </summary>
        /// <param name="pos">Mouse cursor position relative to Canvas.</param>
        public void HandleMouseMove(Point pos)
        {
            if (CurrentTool == SelectedTool.Selection)
            {
                _selection.HandleMouseMove(pos);
            }
            else
            {
                _drawer.HandleMouseMove(pos);
            }
        }

        /// <summary>
        /// Reroutes right click events to appropriate helper class.
        /// </summary>
        public void HandleRightClick(Point pos)
        {
            if (CurrentTool == SelectedTool.Selection)
            {
                _selection.HandleRightClickUp(pos);
            }
            else
            {
                _drawer.HandleSecondaryDraw();
            }
        }

        /// <summary>
        /// Handles key down events by enabling relevant modes.
        /// </summary>
        /// <param name="key">Key that was pressed.</param>
        public void HandleKeyDown(Key key)
        {
            switch (key)
            {
                // all fallthroughs here are intentional
                case Key.LeftShift:
                case Key.RightShift:
                    _drawer.EvenMode = true;
                    _selection.EvenMode = true;
                    break;
                case Key.LeftCtrl:
                case Key.RightCtrl:
                    _selection.MultiSelect = true;
                    break;
            }
        }

        /// <summary>
        /// Handles key up events by disabling relevant modes.
        /// </summary>
        /// <param name="key">Key that was released.</param>
        public void HandleKeyUp(Key key)
        {
            switch (key)
            {
                // all fallthroughs here are intentional
                case Key.LeftShift:
                case Key.RightShift:
                    _drawer.EvenMode = false;
                    _selection.EvenMode = false;
                    break;
                case Key.LeftCtrl:
                case Key.RightCtrl:
                    _selection.MultiSelect = false;
                    break;
            }
        }

        /// <summary>
        /// Handles draw start event by placing the figure directly into
        /// <see cref="CanvasShapes"/> to display on canvas.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Event arguments.</param>
        private void OnDrawStarted(object sender, FigureDrawEventArgs e)
        {
            CanvasShapes.Add(e.AffectedShape);
        }

        /// <summary>
        /// Handles draw cancel event by removing the figure from 
        /// <see cref="CanvasShapes"/>.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Event arguments.</param>
        private void OnDrawCancelled(object sender, FigureDrawEventArgs e)
        {
            CanvasShapes.Remove(e.AffectedShape);
        }

        /// <summary>
        /// Handles draw complete event by moving the figure from
        /// <see cref="CanvasShapes"/> to <see cref="CanvasModel.Figures"/>
        /// as a permanent (for now) addition.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Event arguments.</param>
        private void OnDrawCompleted(object sender, FigureDrawEventArgs e)
        {
            CanvasShapes.Remove(e.AffectedShape);
            var addOp = new AddOperation(_canvas, new[] { e.AffectedShape });
            _undoer.AddOperation(addOp);
        }

        /// <summary>
        /// Handles selection's draw completed event by selecting figures that are
        /// completely within drawn rectangle.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Event arguments containig final version
        /// of selection rectangle.</param>
        private void OnSelectionDrawCompleted(object sender, FigureDrawEventArgs e)
        {
            CanvasShapes.Remove(e.AffectedShape);
            _selection.Selection.RemoveByOne();
            foreach (var figure in _canvas.Figures)
            {
                // this took way much longer than expected, i'm dissapointed at wpf
                if (figure.GetLeft() >= e.AffectedShape.GetLeft()
                    && figure.GetRight() <= e.AffectedShape.GetRight()
                    && figure.GetTop() >= e.AffectedShape.GetTop()
                    && figure.GetBottom() <= e.AffectedShape.GetBottom())
                {
                    _selection.Selection.Add(figure);
                }
            }
        }

        /// <summary>
        /// Clears selection.
        /// </summary>
        public void ClearSelection()
        {
            _selection.Selection.RemoveByOne();
        }

        /// <summary>
        /// Saves artificially rendered snapshot of canvas to PNG image.
        /// </summary>
        /// <param name="path">Path to save to.</param>
        public void SaveCanvasRender(string path)
        {
            // recreating our own copy of canvas from available data
            // instead of using one from the view so this doesn't break mvvm pattern
            // that we're supposed to follow
            var canvas = new Canvas()
            {
                Background = new SolidColorBrush(Colors.White)
            };
            // support figures like markers and selection rect, if present, are not saved
            foreach (var figure in _canvas.Figures)
            {
                // cloning so we don't have to disconnect actual figure
                // from actual canvas just for one picture
                canvas.Children.Add(figure.Clone());
            }
            var r = new Rect(0, 0, _canvas.Width, _canvas.Height);
            canvas.Arrange(r);
            // magic copypasted from the all-knowing internets
            // 96 is probably DPI
            var rtb = new RenderTargetBitmap((int)r.Width, (int)r.Height, 96d, 96d,
                PixelFormats.Default);
            rtb.Render(canvas);
            var pngEncoder = new PngBitmapEncoder();
            pngEncoder.Frames.Add(BitmapFrame.Create(rtb));
            using (MemoryStream ms = new MemoryStream())
            {
                pngEncoder.Save(ms);
                File.WriteAllBytes(path, ms.ToArray());
            }
        }

        /// <summary>
        /// Factory method that creates needed operations and puts them to used
        /// <see cref="OperationManager"/> instance.
        /// </summary>
        /// <param name="opType">Type of operation that was performed. Data is extracted
        /// from <paramref name="states"/> according to this.</param>
        /// <param name="states">Dictonary where value for changed shape is its original
        /// state.</param>
        public void RegisterChange(ChangeOperationType opType,
            Dictionary<Shape, Shape> states)
        {
            // don't scary stuff open inside
            _undoer.AddOperation(OperationFactory.CreateOperation(opType, _canvas,
                states));
        }

        // This one is a special snowflake since it always affects one figure and
        // requries additional info (index).
        /// <summary>
        /// Registers point move operation.
        /// </summary>
        /// <param name="figure">Affected figure.</param>
        /// <param name="original">Affected figure's original state.</param>
        /// <param name="index">Affected point's index.</param>
        public void RegisterPointMove(Shape figure, Shape original, int index)
        {
            if (index < 0)
            {
                throw new ArgumentException("index");
            }
            if (!(figure is Polyline || figure is Polygon))
            {
                throw new ApplicationException(
                    "this only works on polylines and polygons.");
            }
            dynamic f = figure;
            dynamic o = original;
            var dx = f.Points[index].X - o.Points[index].X;
            var dy = f.Points[index].Y - o.Points[index].Y;
            var p = new Point(dx, dy);
            _undoer.AddOperation(new PointMoveOperation(_canvas, (figure, index, p)));
        }

        // the "Power" bit comes from these being an upgrade from (now-removed)
        // original ones that lacked undo/redo history support
        
        // this is my world, I can do anything I want in my world,
        // Bob Ross told me so

        /// <summary>
        /// Saves current state to file.
        /// </summary>
        /// <param name="filename">File to save to.</param>
        private void PowerSaveToFile(string filename)
        {
            if (filename == null)
            {
                throw new ArgumentNullException("filename");
            }
            var context = new StreamingContext(StreamingContextStates.File, _canvas);
            var formatter = new BinaryFormatter(null, context);
            var (done, undone) = _undoer.Export();
            using (var fileStream = new FileStream(filename, FileMode.Create,
                FileAccess.Write))
            {
                formatter.Serialize(fileStream, new SaveState(_canvas.AllFigures,
                    done, undone));
            }
            FixChanges();
        }

        // public because view needs to call that when command-line arguments are passed
        // to the starting application.

        /// <summary>
        /// Loads state from the file.
        /// </summary>
        /// <param name="filename">File to load from. If it is corrupted, app will exit.
        /// </param>
        public void PowerLoadFromFile(string filename)
        {
            if (filename == null)
            {
                throw new ArgumentNullException("filename");
            }
            if (!File.Exists(filename))
            {
                throw new ApplicationException("File does not exist!");
            }
            CreateNew();
            var context = new StreamingContext(StreamingContextStates.File, _canvas);
            var formatter = new BinaryFormatter(null, context);
            SaveState state = null;
            using (var fileStream = new FileStream(filename, FileMode.Open,
                FileAccess.Read))
            {
                try
                {
                    state = (SaveState)formatter.Deserialize(fileStream);
                }
                catch (Exception ex) when (ex is SerializationException ||
                    ex is System.Text.DecoderFallbackException)
                {
                    FailLoad();
                }
            }
            NotifyPropertyChanged("CanvasWidth");
            NotifyPropertyChanged("CanvasHeight");
            try
            {
                foreach (var xamled in state.XamledShapes)
                {
                    _canvas.AllFigures.Add((Shape)XamlReader.Parse(xamled));
                }
            }
            catch (XamlParseException)
            {
                FailLoad();
            }
            _canvas.ShouldRescale = false;
            // yay simulating user input
            foreach (var op in state.Done.Concat(state.Undone))
            {
                _undoer.AddOperation(op);
            }
            for (int i = 0; i < state.Undone.Count; i++)
            {
                UndoCommand.Execute(null);
            }
            _canvas.ShouldRescale = true;
            FixChanges();
            FilePath = filename;
        }

        /// <summary>
        /// Displays a message indicating file load error, then throws an exception.
        /// </summary>
        private void FailLoad()
        {
            MessageBox.Show("Unable to open file.", "Error", MessageBoxButton.OK,
                MessageBoxImage.Error);
            // we're supposed to throw here, the sole purpose of this method is to reuse
            // both of these statements
            throw new ApplicationException("Something went wrong.");
        }

        /// <summary>
        /// Asks user whether they would like to save unsaved changes before doing
        /// something destructive.
        /// </summary>
        /// <returns>True if pending operation can proceed, false otherwise.</returns>
        private bool AskForConfirmation()
        {
            var result = MessageBox.Show("Currently opened file has unsaved changes." +
                "\nWould you like to save it before proceeding?", "Confirmation needed",
                MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
            switch (result)
            {
                //save and continue
                case MessageBoxResult.Yes:
                    SaveCommand.Execute(null);
                    return true;
                // don't save, but continue
                case MessageBoxResult.No:
                    return true;
                // don't continue
                default:
                    return false;
            }
        }

        /// <summary>
        /// Indicates whether it's possible to quit the program. It's possible when there
        /// is no unsaved changes or when the user chose to quit with or without saving.
        /// </summary>
        /// <returns>True if app can exit, false otherwise.</returns>
        public bool ShouldExit()
        {
            return !IsChanged || (IsChanged && AskForConfirmation());
        }

        /// <summary>
        /// Marks current state as an unmodified one saved in the relative file.
        /// </summary>
        private void FixChanges()
        {
            _undoer.MarkAsLastApplied();
            IsChanged = false;
        }

        /// <summary>
        /// Cancel any drag'n'drop preventing it from breaking when mouse button is held
        /// when cursor leaves drawing area.
        /// </summary>
        public void CancelAnyDragging()
        {
            _selection.CancelDragging();
        }
    }
}
