﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;

using Microsoft.Win32;

using VectorAneurysm.DataStructures;
using VectorAneurysm.Models;
using VectorAneurysm.Models.Operations;
using VectorAneurysm.Util;
using VectorAneurysm.Views;

namespace VectorAneurysm.ViewModels
{
    /// <summary>
    /// File to hold the clutter of commands off <see cref="DrawingViewModel"/>.
    /// </summary>
    public partial class DrawingViewModel
    {
        /// <summary>
        /// Filters for file dialogs related to saving in "native format". Promoted to
        /// const because used more than once (yup, twice).
        /// </summary>
        private const string _bvdFilters = "Vector drawings|*.bvd|All files|*.*";

        /// <summary>
        /// Backing field for <see cref="AboutCommand"/> property.
        /// </summary>
        private ICommand _aboutCommand;

        /// <summary>
        /// Command that displays about window.
        /// </summary>
        public ICommand AboutCommand
        {
            // yay lazy initialization
            get => _aboutCommand ??
                (_aboutCommand = new RelayCommand(o => new AboutWindow().ShowDialog()));
        }

        /// <summary>
        /// Backing field for <see cref="CanvasClearCommand"/> property.
        /// </summary>
        private ICommand _canvasClearCommand;

        /// <summary>
        /// Command that clears the canvas, if there is something.
        /// </summary>
        public ICommand CanvasClearCommand
        {
            get => _canvasClearCommand ??
                (_canvasClearCommand = new RelayCommand(
                    o =>
                    {
                        var removal = new RemoveOperation(_canvas, _canvas.Figures);
                        _undoer.AddOperation(removal);
                    },
                    o => _canvas.Figures.Any() && !_drawer.IsDrawing));
        }

        /// <summary>
        /// Backing field for <see cref="CanvasResizeCommand"/> property.
        /// </summary>
        private ICommand _canvasResizeCommand;

        /// <summary>
        /// Command that resizes canvas and everything on it.
        /// </summary>
        public ICommand CanvasResizeCommand
        {
            get => _canvasResizeCommand ??
                (_canvasResizeCommand = new RelayCommand(
                    o => HandleCanvasResize(),
                    o => !_drawer.IsDrawing && !_selection.SelectionDrawer.IsDrawing));
        }

        /// <summary>
        /// Backing field for <see cref="ExitCommand"/> property.
        /// </summary>
        private ICommand _exitCommand;

        /// <summary>
        /// Command that quits the application.
        /// </summary>
        public ICommand ExitCommand
        {
            // asking for confirmation is actually done in Window's closing handler,
            // adding check here results in asking user twice in a row
            get => _exitCommand ??
                (_exitCommand = new RelayCommand(
                    o => Application.Current.Shutdown()
                ));
        }

        /// <summary>
        /// Backing field for <see cref=" FinishFigureCommand"/> property.
        /// </summary>
        private ICommand _finishFigureCommand;

        /// <summary>
        /// Command that finishes drawing, bound to return key.
        /// </summary>
        public ICommand FinishFigureCommand
        {
            get => _finishFigureCommand ??
                (_finishFigureCommand = new RelayCommand(
                    o => _drawer.CompleteDrawing(),
                    o => _drawer.IsDrawing));
        }

        /// <summary>
        /// Backing field for <see cref="CancelFigureCommand"/> property.
        /// </summary>
        private ICommand _cancelFigureCommand;

        /// <summary>
        /// Command that cancels drawing, bound to escape key.
        /// </summary>
        public ICommand CancelFigureCommand
        {
            get => _cancelFigureCommand ??
                (_cancelFigureCommand = new RelayCommand(
                    o =>
                    {
                        if (_drawer.IsDrawing)
                        {
                            _drawer.CancelDrawing();
                        }
                        if (_selection.SelectionDrawer.IsDrawing)
                        {
                            _selection.SelectionDrawer.CancelDrawing();
                        }
                    },
                    o => _drawer.IsDrawing ||
                        _selection.SelectionDrawer.IsDrawing));
        }

        /// <summary>
        /// Backing field for <see cref=" RemoveLastPointCommand"/> property.
        /// </summary>
        private ICommand _removeLastPointCommand;

        /// <summary>
        /// Command that removes last point from polyline or polygon.
        /// </summary>
        public ICommand RemoveLastPointCommand
        {
            get => _removeLastPointCommand ??
                (_removeLastPointCommand = new RelayCommand(
                    o => _drawer.RemoveLastPoint(),
                    o => _drawer.IsDrawing));
        }

        /// <summary>
        /// Backing field for <see cref=" DeleteSelectionCommand"/> property.
        /// </summary>
        private ICommand _deleteSelectionCommand;

        /// <summary>
        /// Command that deletes selected figures for the great justice.
        /// </summary>
        public ICommand DeleteSelectionCommand
        {
            get => _deleteSelectionCommand ??
                (_deleteSelectionCommand = new RelayCommand(
                    o =>
                    {
                        var removal = new RemoveOperation(_canvas, _selection.Selection);
                        _undoer.AddOperation(removal);
                    },
                    o => _selection.Selection.Count > 0));
        }

        /// <summary>
        /// Backing field for <see cref="CutCommand"/> property.
        /// </summary>
        private ICommand _cutCommand;

        /// <summary>
        /// Command that moves selected figures to "clipboard".
        /// </summary>
        public ICommand CutCommand
        {
            get => _cutCommand ??
                (_cutCommand = new RelayCommand(
                    o =>
                    {
                        _canvas.Clipboard.Clear();
                        _canvas.Clipboard.AddRange(_selection.Selection);
                        var removal = new RemoveOperation(_canvas, _selection.Selection);
                        _undoer.AddOperation(removal);
                    },
                    o => _selection.Selection.Count > 0));
        }

        /// <summary>
        /// Backing field for <see cref="PasteCommand"/> property.
        /// </summary>
        private ICommand _pasteCommand;

        /// <summary>
        /// Command that adds "clipboard"'s content to canvas.
        /// </summary>
        public ICommand PasteCommand
        {
            get => _pasteCommand ??
                (_pasteCommand = new RelayCommand(
                    o =>
                    {
                        // tl;dr - newly pasted figures get the selection
                        CurrentTool = SelectedTool.Selection;
                        _selection.Selection.RemoveByOne();
                        foreach (var figure in _canvas.Clipboard)
                        {
                            var clone = figure.Clone();
                            _selection.Selection.Add(clone);
                        }
                        var addOp = new AddOperation(_canvas, _selection.Selection);
                        _undoer.AddOperation(addOp);
                    },
                    o => _canvas.Clipboard.Count > 0));
        }

        /// <summary>
        /// Backing field for <see cref="CopyCommand"/> property.
        /// </summary>
        private ICommand _copyCommand;

        /// <summary>
        /// Command that clones selected figures to "clipboard".
        /// </summary>
        public ICommand CopyCommand
        {
            get => _copyCommand ??
                (_copyCommand = new RelayCommand(
                    o =>
                    {
                        _canvas.Clipboard.Clear();
                        foreach (var figure in _selection.Selection)
                        {
                            _canvas.Clipboard.Add(figure.Clone());
                        }
                    },
                    o => _selection.Selection.Count > 0));
        }

        /// <summary>
        /// Backing field for <see cref="ClearSelectionCommand"/> property.
        /// </summary>
        private ICommand _clearSelectionCommand;

        /// <summary>
        /// Command that explicitly clears selection.
        /// </summary>
        public ICommand ClearSelectionCommand
        {
            get => _clearSelectionCommand ??
                (_clearSelectionCommand = new RelayCommand(
                    o => _selection.Selection.RemoveByOne(),
                    o => _selection.Selection.Count > 0));
        }

        /// <summary>
        /// Backing field for <see cref="SelectAllCommand"/> property.
        /// </summary>
        private ICommand _selectAllCommand;

        /// <summary>
        /// Command that adds everything to selection.
        /// </summary>
        public ICommand SelectAllCommand
        {
            get => _selectAllCommand ??
                (_selectAllCommand = new RelayCommand(
                    o =>
                    {
                        CurrentTool = SelectedTool.Selection;
                        foreach (var figure in _canvas.Figures)
                        {
                            if (!_selection.Selection.Contains(figure))
                            {
                                _selection.Selection.Add(figure);
                            }
                        }
                    },
                    o => _canvas.Figures.Count > 0));
        }

        /// <summary>
        /// Backing field for <see cref="SaveToPngCommand"/> property.
        /// </summary>
        private ICommand _saveToPngCommand;

        /// <summary>
        /// Command that saves current canvas as a raster image.
        /// </summary>
        public ICommand SaveToPngCommand
        {
            get => _saveToPngCommand ??
                (_saveToPngCommand = new RelayCommand(
                    o =>
                    {
                        var dialog = new SaveFileDialog
                        {
                            Filter = "PNG images|*.png|All files|*.*"
                        };
                        if (dialog.ShowDialog() == true)
                        {
                            SaveCanvasRender(dialog.FileName);
                        }
                    }));
        }

        /// <summary>
        /// Backing field for <see cref="UndoCommand"/> property.
        /// </summary>
        private ICommand _undoCommand;

        /// <summary>
        /// Command that rolls back latest canvas change.
        /// </summary>
        public ICommand UndoCommand
        {
            get => _undoCommand ??
                (_undoCommand = new RelayCommand(
                    o =>
                    {
                        bool canvasModified;
                        (IsChanged, canvasModified) = _undoer.Undo();
                        if (canvasModified)
                        {
                            NotifyPropertyChanged(nameof(CanvasWidth));
                            NotifyPropertyChanged(nameof(CanvasHeight));
                        }
                    },
                    o => _undoer.CanUndo && !_selection.Dragging && !_drawer.IsDrawing
                        && !_selection.SelectionDrawer.IsDrawing));
        }

        /// <summary>
        /// Backing field for <see cref="RedoCommand"/> property.
        /// </summary>
        private ICommand _redoCommand;

        /// <summary>
        /// Command that restores previously reverted change.
        /// </summary>
        public ICommand RedoCommand
        {
            get => _redoCommand ??
                (_redoCommand = new RelayCommand(
                    o =>
                    {
                        bool canvasModified;
                        (IsChanged, canvasModified) = _undoer.Redo();
                        if (canvasModified)
                        {
                            NotifyPropertyChanged(nameof(CanvasWidth));
                            NotifyPropertyChanged(nameof(CanvasHeight));
                        }
                    },
                    o => _undoer.CanRedo && !_selection.Dragging && !_drawer.IsDrawing
                        && !_selection.SelectionDrawer.IsDrawing));
        }

        /// <summary>
        /// Backing field for <see cref="NewCommand"/> property.
        /// </summary>
        private ICommand _newCommand;

        /// <summary>
        /// Command that creates new file.
        /// </summary>
        public ICommand NewCommand
        {
            get => _newCommand ??
                (_newCommand = new RelayCommand(
                    o =>
                    {
                        if (!IsChanged || (IsChanged && AskForConfirmation()))
                        {
                            CreateNew();
                        }
                    }));
        }

        /// <summary>
        /// Backing field for <see cref="SaveCommand"/> property.
        /// </summary>
        private ICommand _saveCommand;

        /// <summary>
        /// Command that saves file under its current file name. If there are none,
        /// <see cref="SaveAsCommand"/> is executed instead.
        /// </summary>
        public ICommand SaveCommand
        {
            get => _saveCommand ??
                (_saveCommand = new RelayCommand(
                    o =>
                    {
                        if (FilePath != null)
                        {
                            PowerSaveToFile(FilePath);
                        }
                        else
                        {
                            SaveAsCommand.Execute(null);
                        }
                    },
                    o => IsChanged));
        }

        /// <summary>
        /// Backing field for <see cref="SaveAsCommand"/> property.
        /// </summary>
        private ICommand _saveAsCommand;

        /// <summary>
        /// Command that asks user for file name, then saves under that name.
        /// </summary>
        public ICommand SaveAsCommand
        {
            get => _saveAsCommand ??
                (_saveAsCommand = new RelayCommand(
                    o =>
                    {
                        var saveDialog = new SaveFileDialog()
                        {
                            Filter = _bvdFilters
                        };
                        if (saveDialog.ShowDialog() == true)
                        {
                            FilePath = saveDialog.FileName;
                            PowerSaveToFile(FilePath);
                        }
                    }));
        }

        /// <summary>
        /// Backing field for <see cref="OpenCommand"/> property.
        /// </summary>
        private ICommand _openCommand;

        /// <summary>
        /// Command that loads already existing file.
        /// </summary>
        public ICommand OpenCommand
        {
            get => _openCommand ??
                (_openCommand = new RelayCommand(
                    o =>
                    {
                        if (!IsChanged || (IsChanged && AskForConfirmation()))
                        {
                            var openDialog = new OpenFileDialog()
                            {
                                Filter = _bvdFilters
                            };
                            if (openDialog.ShowDialog() == true)
                            {
                                try
                                {
                                    PowerLoadFromFile(openDialog.FileName);
                                }
                                catch (ApplicationException)
                                {
                                    _exitCommand.Execute(null);
                                }
                            }
                        }
                    }));
        }

        /// <summary>
        /// Backing field for <see cref="SetToolCommand"/> property.
        /// </summary>
        private ICommand _setToolCommand;

        /// <summary>
        /// Command that switches currently selected tool to one specified by passed
        /// parameter.
        /// </summary>
        public ICommand SetToolCommand
        {
            get => _setToolCommand ??
                (_setToolCommand = new RelayCommand(
                    o => CurrentTool = (SelectedTool)o,
                    o => !_selection.Dragging));
        }
    }
}
