﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Shapes;

using VectorAneurysm.Util;

namespace VectorAneurysm.Models
{
    /// <summary>
    /// Class that encapsulates data about canvas and figures on it.
    /// </summary>
    public class CanvasModel
    {
        /// <summary>
        /// Default width value.
        /// </summary>
        public const int DefaultWidth = 800;

        /// <summary>
        /// Default height value.
        /// </summary>
        public const int DefaultHeight = 600;

        /// <summary>
        /// Backing field for <see cref="Width"/> property.
        /// </summary>
        private int _width;

        /// <summary>
        /// Indicates whether figures should be rescaled as well. On most of the time,
        /// turned off for file loading as figures are saved with already scaled size
        /// and each resize operation resizes them more than intended on file loads.
        /// </summary>
        public bool ShouldRescale { get; set; } = true;

        /// <summary>
        /// Canvas width property. Resizes figures proportionately on change.
        /// </summary>
        public int Width
        {
            get => _width;
            set
            {
                if (value <= 0)
                {
                    throw new ApplicationException(
                        "Canvas width must be grater than zero.");
                }
                if (_width != value)
                {
                    var ratioX = value / (double)_width;
                    if (ShouldRescale)
                    {
                        RescaleX(ratioX);
                    }
                    _width = value;
                }
            }
        }

        /// <summary>
        /// Backing field for <see cref="Height"/> property.
        /// </summary>
        private int _height;

        /// <summary>
        /// Canvas height property. Resizes figures proportionately on change.
        /// </summary>
        public int Height
        {
            get => _height;
            set
            {
                if (value <= 0)
                {
                    throw new ApplicationException(
                        "Canvas height must be greater than zero");
                }
                if (_height != value)
                {
                    var ratioY = value / (double)_height;
                    if (ShouldRescale)
                    {
                        RescaleY(ratioY);
                    }
                    _height = value;
                }
            }
        }

        /// <summary>
        /// Holds figures to participate in cut-copy-paste operations.
        /// </summary>
        public List<Shape> Clipboard { get; } = new List<Shape>();

        /// <summary>
        /// Collection of figures draw by user.
        /// </summary>
        public ObservableCollection<Shape> Figures { get; }
            = new ObservableCollection<Shape>();

        /// <summary>
        /// List of all figures ever added to this canvas. Used to point operations to.
        /// </summary>
        public List<Shape> AllFigures { get; } = new List<Shape>();

        /// <summary>
        /// Parameterless constructor, sets up default size.
        /// </summary>
        public CanvasModel() : this(DefaultWidth, DefaultHeight) { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="width">Canvas width.</param>
        /// <param name="height">Canvas height.</param>
        public CanvasModel(int width, int height)
        {
            Width = width;
            Height = height;
        }

        /// <summary>
        /// Changes canvas and figures width.
        /// </summary>
        /// <param name="ratio">Width multiplier.</param>
        private void RescaleX(double ratio)
        {
            foreach (var figure in Figures.Concat(Clipboard))
            {
                figure.SetLeft(figure.GetLeft() * ratio);
                figure.RescaleX(ratio);
            }
        }

        /// <summary>
        /// Changes canvas and figures height.
        /// </summary>
        /// <param name="ratio">Height multiplier.</param>
        private void RescaleY(double ratio)
        {
            foreach(var figure in Figures.Concat(Clipboard))
            {
                figure.SetTop(figure.GetTop() * ratio);
                figure.RescaleY(ratio);
            }
        }
    }
}
