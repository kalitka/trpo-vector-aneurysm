﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Shapes;

using VectorAneurysm.DataStructures;
using VectorAneurysm.ViewModels;

namespace VectorAneurysm.Models
{
    /// <summary>
    /// A class that encapsulates drawing of shapes.
    /// </summary>
    public class ShapeDrawer
    {
        /// <summary>
        /// Event that is fired when drawing begins. Listener is supposed to
        /// start showing the figure passed within this event's argument.
        /// </summary>
        public event EventHandler<FigureDrawEventArgs> DrawStarted;

        /// <summary>
        /// Event that is fired when drawing is cancelled. Listener is supposed
        /// to stop showing and to completely forget the figure passed within argument.
        /// </summary>
        public event EventHandler<FigureDrawEventArgs> DrawCancelled;

        /// <summary>
        /// Event that is fired when drawing is complete. Listener is supposed to
        /// add the figure passed within the argument to list of drawed figures.
        /// </summary>
        public event EventHandler<FigureDrawEventArgs> DrawCompleted;

        /// <summary>
        /// Field to store affected figure and to give references to in events arguments.
        /// </summary>
        private Shape _currentlyBeingDrawn = null;

        /// <summary>
        /// Stores reference to used viewmodel to clamp drawed figures within.
        /// </summary>
        private readonly DrawingViewModel _parent;

        /// <summary>
        /// Shape that drawed figures borrow their parameters.
        /// </summary>
        private readonly Shape _palette;

        /// <summary>
        /// Used to keep track of where painting of current figure started.
        /// </summary>
        private Point? _drawStartPoint;

        /// <summary>
        /// Indicates whether there is a figure being drawn right now.
        /// </summary>
        public bool IsDrawing { get; private set; }

        /// <summary>
        /// Indicates whether figures produced will have equal dimensions and/or
        /// straight vertical/horizontal lines.
        /// </summary>
        public bool EvenMode { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parent">Viewmodel info to use.</param>
        /// <param name="paletteOverride">Optional override of the <paramref name="parent"/>
        /// 's palette as needed by selection's drawer.</param>
        public ShapeDrawer(DrawingViewModel parent, Shape paletteOverride = null)
        {
            _parent = parent ?? throw new ArgumentNullException("parent");
            _palette = paletteOverride ?? parent.Palette;
        }

        /// <summary>
        /// Handles left click on a canvas, whether it is used to start a new figure,
        /// finish existing rect/ellipse or add a point to polygon/polyline.
        /// </summary>
        /// <param name="position">Mouse position relative to canvas.</param>
        /// <param name="tool">Currently selected tool. Couldn't think of a better way
        /// to pass this from viewmodel pepeHands.</param>
        public void HandlePrimaryDraw(Point position, SelectedTool tool)
        {
            if (IsDrawing)
            {
                // finish rect/ellipse, add point to polywhatever
                if (IsDrawingFromCornerToCorner())
                {
                    FinishDrawing(true);
                }
                else
                {
                    dynamic typed = _currentlyBeingDrawn;
                    typed.Points.Add(new Point() { X = position.X, Y = position.Y });
                }
            }
            else
            {
                if (position.X >= 0 && position.X <= _parent.CanvasWidth &&
                    position.Y >= 0 && position.Y <= _parent.CanvasHeight)
                {
                    _drawStartPoint = position;
                    _currentlyBeingDrawn = ShapeFactory.GetRelevantShape(tool);
                    IsDrawing = true;
                    BindToPalette();
                    if (IsDrawingFromCornerToCorner())
                    {
                        Canvas.SetLeft(_currentlyBeingDrawn, position.X);
                        Canvas.SetTop(_currentlyBeingDrawn, position.Y);
                    }
                    else
                    {
                        dynamic typed = _currentlyBeingDrawn;
                        for (int i = 0; i < 2; i++)
                        {
                            typed.Points.Add(position);
                        }
                    }
                    OnDrawStarted(new FigureDrawEventArgs(_currentlyBeingDrawn));
                }
            }
        }

        /// <summary>
        /// Handles mouse move event by appropriately resizing drawn figure, if any.
        /// </summary>
        /// <param name="position"></param>
        public void HandleMouseMove(Point position)
        {

            position.X = OddsEnds.Clamp(position.X, 0, _parent.CanvasWidth);
            position.Y = OddsEnds.Clamp(position.Y, 0, _parent.CanvasHeight);

            if (IsDrawing && _drawStartPoint.HasValue)
            {
                if (IsDrawingFromCornerToCorner())
                {
                    var newLeft = Math.Min(position.X, _drawStartPoint.Value.X);
                    var newTop = Math.Min(position.Y, _drawStartPoint.Value.Y);

                    var width = Math.Abs(position.X - _drawStartPoint.Value.X);
                    var height = Math.Abs(position.Y - _drawStartPoint.Value.Y);

                    if (EvenMode)
                    {
                        width = height = Math.Max(width, height);
                    }

                    _currentlyBeingDrawn.Width = width;
                    _currentlyBeingDrawn.Height = height;
                    Canvas.SetLeft(_currentlyBeingDrawn, newLeft);
                    Canvas.SetTop(_currentlyBeingDrawn, newTop);

                    if (newLeft + width > _parent.CanvasWidth)
                    {
                        _currentlyBeingDrawn.Width
                            -= newLeft + width - _parent.CanvasWidth;
                    }
                    if (newTop + height > _parent.CanvasHeight)
                    {
                        _currentlyBeingDrawn.Height
                            -= newTop + height - _parent.CanvasHeight;
                    }

                }
                else
                {
                    dynamic typed = _currentlyBeingDrawn;

                    if (EvenMode)
                    {
                        Point previous = typed.Points[typed.Points.Count - 2];

                        var deltaX = position.X - previous.X;
                        var deltaY = position.Y - previous.Y;

                        if (Math.Abs(deltaX) > Math.Abs(deltaY))
                        {
                            position.Y = previous.Y;
                        }
                        else
                        {
                            position.X = previous.X;
                        }
                    }
                    typed.Points[typed.Points.Count - 1] = position;
                }
            }
        }

        /// <summary>
        /// Handles right click event by finishing drawing the figure, if it was drawn.
        /// </summary>
        public void HandleSecondaryDraw()
        {
            if (IsDrawing)
            {
                FinishDrawing(true);
            }
        }

        /// <summary>
        /// Handles return keypress by finishing drawing the figure, if it was drawn.
        /// </summary>
        public void CompleteDrawing()
        {
            if (IsDrawing)
            {
                // extra functionality for lines: close the contour
                if (_currentlyBeingDrawn is Polyline p)
                {
                    p.Points.Add(p.Points[0]);
                }
                FinishDrawing(true);
            }
        }

        /// <summary>
        /// Handles backspace keypress -- removes last point from drawn polyline/polygon,
        /// if there is at least 3, otherwise does literally nothing.
        /// </summary>
        public void RemoveLastPoint()
        {
            if (IsDrawing && 
                (_currentlyBeingDrawn is Polygon || _currentlyBeingDrawn is Polyline))
            {
                dynamic typed = _currentlyBeingDrawn;
                if (typed.Points.Count > 2)
                {
                    typed.Points.RemoveAt(typed.Points.Count - 1);
                }
            }
        }

        /// <summary>
        /// Handles escape keypress by cancelling the current drawing, if any.
        /// </summary>
        public void CancelDrawing()
        {
            if (IsDrawing)
            {
                FinishDrawing(false);
            }
        }

        /// <summary>
        /// Fires <see cref="DrawStarted"/> event.
        /// </summary>
        /// <param name="e">Event arguments to pass with the event.</param>
        private void OnDrawStarted(FigureDrawEventArgs e)
        {
            DrawStarted?.Invoke(this, e);
        }

        /// <summary>
        /// Fires <see cref="DrawCancelled"/> event.
        /// </summary>
        /// <param name="e">Event arguments to pass with the event.</param>
        private void OnDrawCancelled(FigureDrawEventArgs e)
        {
            DrawCancelled?.Invoke(this, e);
        }

        /// <summary>
        /// Fires <see cref="DrawCompleted"/> event.
        /// </summary>
        /// <param name="e">Event arguments to pass with the event.</param>
        private void OnDrawCompleted(FigureDrawEventArgs e)
        {
            DrawCompleted?.Invoke(this, e);
        }

        /// <summary>
        /// Binds <see cref="_currentlyBeingDrawn"/> properties to palette
        /// so parameters could be changed from UI.
        /// </summary>
        private void BindToPalette()
        {
            // just in case
            BindingOperations.ClearAllBindings(_currentlyBeingDrawn);
            // this is uglier than I expected, but
            var propertyNames = new Dictionary<string, DependencyProperty>()
            {
                ["Stroke"] = Shape.StrokeProperty,
                ["StrokeThickness"] = Shape.StrokeThicknessProperty,
                ["StrokeDashArray"] = Shape.StrokeDashArrayProperty
            };
            if (!(_currentlyBeingDrawn is Polyline))
            {
                propertyNames["Fill"] = Shape.FillProperty;
            }
            foreach (var name in propertyNames)
            {
                var b = new Binding
                {
                    Source = _palette,
                    Path = new PropertyPath(name.Key)
                };
                BindingOperations.SetBinding(_currentlyBeingDrawn, name.Value, b);
            }
        }

        /// <summary>
        /// Helper method that sets <see cref="_currentlyBeingDrawn"/> properties to
        /// its unbound clones as values reset with the binding.
        /// </summary>
        private void UnbindFromPalette()
        {
            var fill = _currentlyBeingDrawn.Fill.CloneCurrentValue();
            var stroke = _currentlyBeingDrawn.Stroke.CloneCurrentValue();
            var strokeThickness = _currentlyBeingDrawn.StrokeThickness;
            var strokeDashArray
                = _currentlyBeingDrawn.StrokeDashArray?.CloneCurrentValue();

            BindingOperations.ClearAllBindings(_currentlyBeingDrawn);

            _currentlyBeingDrawn.Fill = fill;
            _currentlyBeingDrawn.Stroke = stroke;
            _currentlyBeingDrawn.StrokeThickness = strokeThickness;
            _currentlyBeingDrawn.StrokeDashArray = strokeDashArray;
        }

        /// <summary>
        /// Checks whether current figure is drawn in two clicks from corner to corner.
        /// </summary>
        /// <returns>True if we're drawing Ellipse or Rectangle,
        /// false otherwise.</returns>
        private bool IsDrawingFromCornerToCorner()
        {
            return _currentlyBeingDrawn is Ellipse || _currentlyBeingDrawn is Rectangle;
        }

        /// <summary>
        /// Finishes figure drawing.
        /// </summary>
        /// <param name="saveResults">Whether figure should be saved.</param>
        private void FinishDrawing(bool saveResults)
        {
            if (saveResults)
            {
                // polygons with <3 points are not valid and should be removed
                if (_currentlyBeingDrawn is Polygon p && p.Points.Count < 3)
                {
                    OnDrawCancelled(new FigureDrawEventArgs(_currentlyBeingDrawn));
                }
                else
                {
                    OnDrawCompleted(new FigureDrawEventArgs(_currentlyBeingDrawn));
                }
            }
            else
            {
                OnDrawCancelled(new FigureDrawEventArgs(_currentlyBeingDrawn));
            }
            UnbindFromPalette();
            _currentlyBeingDrawn = null;
            IsDrawing = false;
            _drawStartPoint = null;
        }
    }
}
