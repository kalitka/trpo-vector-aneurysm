﻿using System;
using System.Collections.Generic;
using System.Windows.Shapes;

using VectorAneurysm.DataStructures;
using VectorAneurysm.Models.Operations;

namespace VectorAneurysm.Models
{
    /// <summary>
    /// Class that hides away the scary guts of extracting needed data from figures
    /// to create undoable operations.
    /// </summary>
    public static class OperationFactory
    {
        /// <summary>
        /// Factory method that creates needed operations and puts them to used
        /// <see cref="OperationManager"/> instance. Note that PointMove-, Add-, and
        /// RemoveOperations are created elsewhere and ResizeOperation is not created
        /// directly, only as a part of ResizeMove.
        /// </summary>
        /// <param name="opType">Type of operation that was performed. Data is extracted
        /// from <paramref name="states"/> according to this.</param>
        /// <param name="canvas"><see cref="CanvasModel"/> operation will affect.</param>
        /// <param name="states">Dictonary where value for changed shape is its original
        /// state.</param>
        public static OperationBase CreateOperation(ChangeOperationType opType,
            CanvasModel canvas, Dictionary<Shape, Shape> states)
        {
            // yay autogeneration
            if (states == null)
            {
                throw new ArgumentNullException(nameof(states));
            }
            switch (opType)
            {
                case ChangeOperationType.Moved:
                    return new MoveOperation(canvas, states);
                case ChangeOperationType.ResizedMoved:
                    return new ResizeMoveOperation(canvas, states);
                case ChangeOperationType.ThicknessChanged:
                    return new ThicknessChangedOperation(canvas, states);
                case ChangeOperationType.DashStyleChanged:
                    return new StrokeTypeChangedOperation(canvas, states);
                case ChangeOperationType.FillChanged:
                    return new FillColorChangedOperation(canvas, states);
                case ChangeOperationType.StrokeChanged:
                    return new StrokeColorChangedOperation(canvas, states);
                // other operations use other methods of creation elsewhere
                default:
                    throw new ApplicationException("Unsupported opeartion type!");
            }
        }
    }
}
