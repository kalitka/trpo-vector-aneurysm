﻿using System.Collections.Generic;

using VectorAneurysm.DataStructures;

namespace VectorAneurysm.Models
{
    /// <summary>
    /// Class whose sole responsibility is generation of list
    /// of possible line stroke types.
    /// </summary>
    public static class StrokeTypesFactory
    {
        /// <summary>
        /// Method to get all available stroke patterns. New ones should be defined here.
        /// </summary>
        /// <returns>List of useable stroke patterns.</returns>
        public static IEnumerable<StrokeTypeItem> GetStrokeTypes()
        {
            yield return new StrokeTypeItem("Solid");
            yield return new StrokeTypeItem("Dashed", 3, 1);
            yield return new StrokeTypeItem("Long dashed", 7, 1);
            yield return new StrokeTypeItem("Dotted", 1, 1);
            yield return new StrokeTypeItem("Dashed-dotted", 3, 1, 1, 1);
        }
    }
}
