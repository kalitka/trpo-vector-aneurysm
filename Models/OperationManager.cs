﻿using System;
using System.Collections.Generic;
using System.Linq;

using VectorAneurysm.Models.Operations;

namespace VectorAneurysm.Models
{
    /// <summary>
    /// Class that encapsulates undo and redo of canvas' figures manipulation.
    /// </summary>
    public class OperationManager
    {
        /// <summary>
        /// Stack of operations that can be undone. Top operation is the last one added.
        /// </summary>
        private readonly Stack<OperationBase> _undoStack = new Stack<OperationBase>();

        /// <summary>
        /// Stack of operations that can be redone. Top operation is the last one undone.
        /// </summary>
        private readonly Stack<OperationBase> _redoStack = new Stack<OperationBase>();

        /// <summary>
        /// Indicates whether this instance has something to undo.
        /// </summary>
        public bool CanUndo => _undoStack.Count > 0;

        /// <summary>
        /// Indicates whether this instance has something to redo.
        /// </summary>
        public bool CanRedo => _redoStack.Count > 0;

        /// <summary>
        /// Emitted when new operation is added. Used to centralize setting of
        /// changed flag in the wiewmodel since
        /// </summary>
        public event EventHandler OnOperartionAdded;

        /// <summary>
        /// Extra action to perform every undo and redo. Updates selection markers.
        /// </summary>
        private readonly Action _extraAction;

        /// <summary>
        /// Reference to an operation applying of which will restore file to state it
        /// was on file loading.
        /// </summary>
        private OperationBase _lastAppliedOnSaved;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="action">Additional action to perform every undo/redo.</param>
        public OperationManager(Action action = null)
        {
            _extraAction = action;
        }

        /// <summary>
        /// Emits the <see cref="OnOperartionAdded"/> event.
        /// </summary>
        private void EmitOperationAdded()
        {
            // a hack to "pass" a "boolean" on whether canvas size was modified:
            // a new instance if yes, empty args otherwise
            var args = SafelyGetTop() is CanvasResizeOperation ?
                new EventArgs() : EventArgs.Empty;
            OnOperartionAdded?.Invoke(this, args);
        }

        /// <summary>
        /// Applies operation and makes it possible to undo it.
        /// </summary>
        /// <param name="op">Operation to add.</param>
        public void AddOperation(OperationBase op)
        {
            if (op.ApplyOnAdd)
            {
                op.Apply();
            }
            _undoStack.Push(op);
            // if there was something, it's lost forever (a long long time)
            _redoStack.Clear();
            EmitOperationAdded();
        }

        /// <summary>
        /// Undoes last change done.
        /// </summary>
        /// <returns>Pair of booleans: whether file is now in modified state, and
        /// whether this call affected canvas's size.</returns>
        public (bool IsModified, bool WasCanvasChanged) Undo()
        {
            if (!CanUndo)
            {
                throw new InvalidOperationException("Nothing to revert!");
            }
            var undone = _undoStack.Pop();
            undone.Revert();
            _extraAction?.Invoke();
            _redoStack.Push(undone);
            return (SafelyGetTop() != _lastAppliedOnSaved, 
                undone is CanvasResizeOperation);
        }

        /// <summary>
        /// Restores last undone change.
        /// </summary>
        /// <returns>Pair of booleans: whether file is now in modified state, and
        /// whether this call affected canvas's size.</returns>
        public (bool IsModified, bool WasCanvasChanged) Redo()
        {
            if (!CanRedo)
            {
                throw new InvalidOperationException("Nothing to restore!");
            }
            var redone = _redoStack.Pop();
            redone.Apply();
            _extraAction?.Invoke();
            _undoStack.Push(redone);
            return (SafelyGetTop() != _lastAppliedOnSaved,
                redone is CanvasResizeOperation);
        }

        /// <summary>
        /// Resets this to contain no operations at all.
        /// </summary>
        public void Clear()
        {
            _undoStack.Clear();
            _redoStack.Clear();
            _lastAppliedOnSaved = null;
        }

        /// <summary>
        /// Marks last done operation as one which application will restore the file
        /// to the saved state.
        /// </summary>
        public void MarkAsLastApplied()
        {
            _lastAppliedOnSaved = SafelyGetTop();
        }

        /// <summary>
        /// Exports operation stack for saving to file.
        /// </summary>
        /// <returns>Tuple of <see cref="_undoStack"/> and <see cref="_redoStack"/>
        /// as read-only enumerables.</returns>
        public (IReadOnlyCollection<OperationBase>,
            IReadOnlyCollection<OperationBase>) Export()
        {
            return (_undoStack.ToList().AsReadOnly(),
                _redoStack.ToList().AsReadOnly());
        }

        /// <summary>
        /// Safely (without bursting into exceptions about stack being empty) gets
        /// last applied operation.
        /// </summary>
        /// <returns>Operation on top of <see cref="_undoStack"/> or null, if stack's
        /// empty.</returns>
        private OperationBase SafelyGetTop()
        {
            return _undoStack.Any() ? _undoStack.Peek() : null;
        }
    }
}
