﻿using System.Windows.Shapes;

using VectorAneurysm.DataStructures;
using VectorAneurysm.Util;
using VectorAneurysm.ViewModels;

namespace VectorAneurysm.Models.Markers
{
    /// <summary>
    /// Represents a marker in bottom right angle of the figure that moves both right and
    /// bottom edges of a figure.
    /// </summary>
    public class BottomRightMarker : MarkerBase
    {
        /// <summary>
        /// Dimension of every side of square marker.
        /// </summary>
        protected override double Size => _sizeBig;

        /// <summary>
        /// Indicates figure's right edge is linked to and moves with this marker.
        /// </summary>
        public override HorizontalAnchor HorizontalAnchor => HorizontalAnchor.Right;

        /// <summary>
        /// Indicates figure's bottom edge is linked to and moves with this marker.
        /// </summary>
        public override VerticalAnchor VerticalAnchor => VerticalAnchor.Bottom;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parent">Figure this marker resizes.</param>
        /// <param name="viewmodel">Viewmodel this marker belongs to.</param>
        public BottomRightMarker(Shape parent, DrawingViewModel viewmodel)
            : base(parent, viewmodel) { }

        /// <summary>
        /// Moves marker to its position over the bottom-right corner of related figure.
        /// </summary>
        public override void UpdatePosition()
        {
            SetCenter(Parent.GetRight(), Parent.GetBottom());
        }
    }
}
