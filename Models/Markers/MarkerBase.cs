﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

using VectorAneurysm.DataStructures;
using VectorAneurysm.Util;
using VectorAneurysm.ViewModels;

namespace VectorAneurysm.Models.Markers
{
    /// <summary>
    /// Base class for all resize markers.
    /// </summary>
    public abstract class MarkerBase
    {
        /// <summary>
        /// Small size used on points markers.
        /// </summary>
        protected const double _sizeSmall = 8d;

        /// <summary>
        /// Medium size used on markers that scale one dimension.
        /// </summary>
        protected const double _sizeMedium = 12d;

        /// <summary>
        /// Big size used on markers that scale two dimensions.
        /// </summary>
        protected const double _sizeBig = 16d;

        /// <summary>
        /// Backing field to <see cref="Marker"/> property.
        /// </summary>
        protected readonly Rectangle _marker;

        /// <summary>
        /// Shape used to represent this marker in UI.
        /// </summary>
        public Rectangle Marker => _marker;

        /// <summary>
        /// Reference to the figure this marker affects.
        /// </summary>
        public Shape Parent { get; }

        /// <summary>
        /// Position of center of marker.
        /// </summary>
        public Point Center
            => new Point(_marker.GetLeft() + Size / 2, _marker.GetTop() + Size / 2);

        /// <summary>
        /// Sets up how marker affects horizontal scaling of the figure.
        /// </summary>
        public abstract HorizontalAnchor HorizontalAnchor { get; }

        /// <summary>
        /// Sets up how marker affects vertical scaling of the figure.
        /// </summary>
        public abstract VerticalAnchor VerticalAnchor { get; }

        /// <summary>
        /// Size of the marker.
        /// </summary>
        protected abstract double Size { get; }

        /// <summary>
        /// Used <see cref="DrawingViewModel"/> to clamp markers within.
        /// </summary>
        protected readonly DrawingViewModel _viewmodel;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parent">Figure this marker resizes.</param>
        /// <param name="viewmodel">Viewmodel this marker belongs to.</param>
        public MarkerBase(Shape parent, DrawingViewModel viewmodel)
        {
            Parent = parent ?? throw new ArgumentNullException("parent");
            _viewmodel = viewmodel ?? throw new ArgumentNullException("viewmodel");
            _marker = new Rectangle
            {
                Width = Size,
                Height = Size,
                Fill = new SolidColorBrush(Color.FromRgb(255, 255, 255)),
                Stroke = new SolidColorBrush(Color.FromRgb(0, 0, 0)),
                StrokeThickness = 1
            };
            UpdatePosition();
        }

        /// <summary>
        /// Sets the center of marker to given coordinates, provided they're
        /// inside the canvas.
        /// </summary>
        /// <param name="x">X coordinate to set.</param>
        /// <param name="y">Y coordinate to set.</param>
        public void SetCenter(double x, double y)
        {
            var halfSize = Size / 2;
            x = OddsEnds.Clamp(x - halfSize, -halfSize,
                _viewmodel.CanvasWidth - halfSize);
            y = OddsEnds.Clamp(y - halfSize, -halfSize,
                _viewmodel.CanvasHeight - halfSize);
            Canvas.SetLeft(_marker, x);
            Canvas.SetTop(_marker, y);
        }

        /// <summary>
        /// Moves marker to its position over related figure.
        /// </summary>
        public abstract void UpdatePosition();
    }
}
