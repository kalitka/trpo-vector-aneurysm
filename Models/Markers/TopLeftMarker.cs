﻿using System.Windows.Shapes;

using VectorAneurysm.DataStructures;
using VectorAneurysm.Util;
using VectorAneurysm.ViewModels;

namespace VectorAneurysm.Models.Markers
{
    /// <summary>
    /// Represents a marker in top-left angle of the figure that moves both left and top
    /// edges of a figure.
    /// </summary>
    public class TopLeftMarker : MarkerBase
    {
        /// <summary>
        /// Dimension of every side of square marker.
        /// </summary>
        protected override double Size => _sizeBig;

        /// <summary>
        /// Indicates figure's left edge is linked to and moves with this marker.
        /// </summary>
        public override HorizontalAnchor HorizontalAnchor => HorizontalAnchor.Left;

        /// <summary>
        /// Indicates figure's top edge is linked to and moves with this marker.
        /// </summary>
        public override VerticalAnchor VerticalAnchor => VerticalAnchor.Top;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parent">Figure this marker resizes.</param>
        /// <param name="viewmodel">Viewmodel this marker belongs to.</param>
        public TopLeftMarker(Shape parent, DrawingViewModel viewmodel)
            : base(parent, viewmodel) { }

        /// <summary>
        /// Moves marker to its position over the top-left corner of related figure.
        /// </summary>
        public override void UpdatePosition()
        {
            SetCenter(Parent.GetLeft(), Parent.GetTop());
        }
    }
}
