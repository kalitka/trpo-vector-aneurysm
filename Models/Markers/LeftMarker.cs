﻿using System.Windows.Shapes;

using VectorAneurysm.DataStructures;
using VectorAneurysm.Util;
using VectorAneurysm.ViewModels;

namespace VectorAneurysm.Models.Markers
{
    /// <summary>
    /// Represents a marker in the middle of left edge of the figure that moves
    /// left edge of the figure.
    /// </summary>
    public class LeftMarker : MarkerBase
    {
        /// <summary>
        /// Dimension of every side of square marker.
        /// </summary>
        protected override double Size => _sizeMedium;

        /// <summary>
        /// Indicates figure's left edge is linked to and moves with this marker.
        /// </summary>
        public override HorizontalAnchor HorizontalAnchor => HorizontalAnchor.Left;

        /// <summary>
        /// Indicates this marker does not affect vertical scaling.
        /// </summary>
        public override VerticalAnchor VerticalAnchor => VerticalAnchor.None;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parent">Figure this marker resizes.</param>
        /// <param name="viewmodel">Viewmodel this marker belongs to.</param>
        public LeftMarker(Shape parent, DrawingViewModel viewmodel)
            : base(parent, viewmodel) { }

        /// <summary>
        /// Moves marker to its position over the middle of left edge of related figure.
        /// </summary>
        public override void UpdatePosition()
        {
            SetCenter(Parent.GetLeft(), (Parent.GetTop() + Parent.GetBottom()) / 2);
        }
    }
}
