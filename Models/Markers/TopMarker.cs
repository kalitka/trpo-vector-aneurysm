﻿using System.Windows.Shapes;

using VectorAneurysm.DataStructures;
using VectorAneurysm.Util;
using VectorAneurysm.ViewModels;

namespace VectorAneurysm.Models.Markers
{
    /// <summary>
    /// Represents a marker in the middle of top edge of the figure that moves
    /// top edge of the figure.
    /// </summary>
    public class TopMarker : MarkerBase
    {
        /// <summary>
        /// Dimension of every side of square marker.
        /// </summary>
        protected override double Size => _sizeMedium;

        /// <summary>
        /// Indicates this marker does not affect horizontal scaling.
        /// </summary>
        public override HorizontalAnchor HorizontalAnchor => HorizontalAnchor.None;

        /// <summary>
        /// Indicates figure's top edge is linked to and moves with this marker.
        /// </summary>
        public override VerticalAnchor VerticalAnchor => VerticalAnchor.Top;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parent">Figure this marker resizes.</param>
        /// <param name="viewmodel">Viewmodel this marker belongs to.</param>
        public TopMarker(Shape parent, DrawingViewModel viewmodel)
            : base(parent, viewmodel) { }

        /// <summary>
        /// Moves marker to its position over the middle of top edge of related figure.
        /// </summary>
        public override void UpdatePosition()
        {
            SetCenter((Parent.GetLeft() + Parent.GetRight()) / 2, Parent.GetTop());
        }
    }
}
