﻿using System.Windows.Shapes;

using VectorAneurysm.DataStructures;
using VectorAneurysm.Util;
using VectorAneurysm.ViewModels;

namespace VectorAneurysm.Models.Markers
{
    /// <summary>
    /// Represents a marker in the middle of bottom edge of the figure that moves
    /// bottom edge of the figure.
    /// </summary>
    public class BottomMarker : MarkerBase
    {
        /// <summary>
        /// Dimension of every side of square marker.
        /// </summary>
        protected override double Size => _sizeMedium;

        /// <summary>
        /// Indicates this marker does not affect horizontal scaling.
        /// </summary>
        public override HorizontalAnchor HorizontalAnchor => HorizontalAnchor.None;

        /// <summary>
        /// Indicates figure's bottom edge is linked to and moves with this marker.
        /// </summary>
        public override VerticalAnchor VerticalAnchor => VerticalAnchor.Bottom;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parent">Figure this marker resizes.</param>
        /// <param name="viewmodel">Viewmodel this marker belongs to.</param>
        public BottomMarker(Shape parent, DrawingViewModel viewmodel)
            : base(parent, viewmodel) { }

        /// <summary>
        /// Moves marker to its position over the middle of bottom edge of related figure.
        /// </summary>
        public override void UpdatePosition()
        {
            SetCenter((Parent.GetLeft() + Parent.GetRight()) / 2, Parent.GetBottom());
        }
    }
}
