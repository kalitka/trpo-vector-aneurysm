﻿using System.Windows.Shapes;

using VectorAneurysm.DataStructures;
using VectorAneurysm.Util;
using VectorAneurysm.ViewModels;

namespace VectorAneurysm.Models.Markers
{
    /// <summary>
    /// Represents a marker in bottom-left angle of the figure that moves both left and
    /// bottom edges of a figure.
    /// </summary>
    public class BottomLeftMarker : MarkerBase
    {
        /// <summary>
        /// Dimension of every side of square marker.
        /// </summary>
        protected override double Size => _sizeBig;

        /// <summary>
        /// Indicates figure's left edge is linked to and moves with this marker.
        /// </summary>
        public override HorizontalAnchor HorizontalAnchor => HorizontalAnchor.Left;

        /// <summary>
        /// Indicates figure's bottom edge is linked to and moves with this marker.
        /// </summary>
        public override VerticalAnchor VerticalAnchor => VerticalAnchor.Bottom;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parent">Figure this marker resizes.</param>
        /// <param name="viewmodel">Viewmodel this marker belongs to.</param>
        public BottomLeftMarker(Shape parent, DrawingViewModel viewmodel)
            : base(parent, viewmodel) { }

        /// <summary>
        /// Moves marker to its position over the bottom-left corner of related figure.
        /// </summary>
        public override void UpdatePosition()
        {
            SetCenter(Parent.GetLeft(), Parent.GetBottom());
        }
    }
}
