﻿using System;
using System.Windows;
using System.Windows.Shapes;

using VectorAneurysm.DataStructures;
using VectorAneurysm.ViewModels;

namespace VectorAneurysm.Models.Markers
{
    /// <summary>
    /// Represents marker that moves single points of polylines/polygons.
    /// </summary>
    public class PointMarker : MarkerBase
    {
        /// <summary>
        /// Dimension of every side of square marker.
        /// </summary>
        protected override double Size => _sizeSmall;

        /// <summary>
        /// Zero-based index of point this marker associated with.
        /// </summary>
        public int Index { get; private set; }

        // this marker is indeed special as it does not affect whole figure at all.

        /// <summary>
        /// Indicates this marker does not affect horizontal scaling.
        /// </summary>
        public override HorizontalAnchor HorizontalAnchor => HorizontalAnchor.None;

        /// <summary>
        /// Indicates this marker does not affect vertical scaling.
        /// </summary>
        public override VerticalAnchor VerticalAnchor => VerticalAnchor.None;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parent">Figure this marker resizes.</param>
        /// <param name="viewmodel">Viewmodel this marker belongs to.</param>
        /// <param name="pointIndex">Index of point to snap to.</param>
        public PointMarker(Shape parent, DrawingViewModel viewmodel, int pointIndex)
            : base(parent, viewmodel)
        {
            if (!(parent is Polyline || parent is Polygon))
            {
                throw new ArgumentException("parent");
            }
            Index = pointIndex;
            // this call depends on index not set in base ctor, so is repeated here
            UpdatePosition();
        }

        /// <summary>
        /// Moves marker to its position over associated point.
        /// </summary>
        public override void UpdatePosition()
        {
            dynamic d = Parent;
            Point p = d.Points[Index];
            SetCenter(p.X, p.Y);
        }
    }
}
