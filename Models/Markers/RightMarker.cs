﻿using System.Windows.Shapes;

using VectorAneurysm.DataStructures;
using VectorAneurysm.Util;
using VectorAneurysm.ViewModels;

namespace VectorAneurysm.Models.Markers
{
    /// <summary>
    /// Represents a marker in the middle of right edge of the figure that moves
    /// right edge of the figure.
    /// </summary>
    public class RightMarker : MarkerBase
    {
        /// <summary>
        /// Dimension of every side of square marker.
        /// </summary>
        protected override double Size => _sizeMedium;

        /// <summary>
        /// Indicates figure's right edge is linked to and moves with this marker.
        /// </summary>
        public override HorizontalAnchor HorizontalAnchor => HorizontalAnchor.Right;

        /// <summary>
        /// Indicates this marker does not affect vertical scaling.
        /// </summary>
        public override VerticalAnchor VerticalAnchor => VerticalAnchor.None;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parent">Figure this marker resizes.</param>
        /// <param name="viewmodel">Viewmodel this marker belongs to.</param>
        public RightMarker(Shape parent, DrawingViewModel viewmodel)
            : base(parent, viewmodel) { }

        /// <summary>
        /// Moves marker to its position over the middle of right edge of related figure.
        /// </summary>
        public override void UpdatePosition()
        {
            SetCenter(Parent.GetRight(), (Parent.GetTop() + Parent.GetBottom()) / 2);
        }
    }
}
