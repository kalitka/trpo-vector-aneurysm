﻿using System;
using System.Windows.Media;
using System.Windows.Shapes;

namespace VectorAneurysm.DataStructures
{
    /// <summary>
    /// Factory class to create Shape instances in respect to <see cref="SelectedTool"/>.
    /// </summary>
    public static class ShapeFactory
    {
        /// <summary>
        /// Factory method.
        /// </summary>
        /// <param name="tool">Tool to get Shape for.</param>
        /// <returns>Somewhat configured Shape instance.</returns>
        public static Shape GetRelevantShape(SelectedTool tool)
        {
            Shape ret;
            switch(tool)
            {
                case SelectedTool.Line:
                    ret = new Polyline();
                    break;
                case SelectedTool.Polygon:
                    ret = new Polygon();
                    break;
                case SelectedTool.Ellipse:
                    ret = new Ellipse();
                    break;
                case SelectedTool.Rectangle:
                    ret = new Rectangle();
                    break;
                // fallthrough is intentional
                case SelectedTool.Selection:
                default:
                    throw new ApplicationException("Invalid SelectedTool value.");
            }
            // basic setup
            ret.Fill = new SolidColorBrush();
            // polylines always have transparent fill and rounded corners
            if (ret is Polyline)
            {
                (ret.Fill as SolidColorBrush).Color = Color.FromArgb(0, 0, 0, 0);
                ret.StrokeLineJoin = PenLineJoin.Round;
            }
            ret.Stroke = new SolidColorBrush();

            return ret;
        }
    }
}
