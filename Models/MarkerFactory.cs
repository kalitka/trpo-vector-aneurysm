﻿using System.Collections.Generic;
using System.Windows.Shapes;

using VectorAneurysm.Models.Markers;
using VectorAneurysm.ViewModels;

namespace VectorAneurysm.Models
{
    /// <summary>
    /// Class that creates all the markers for figures.
    /// </summary>
    public static class MarkerFactory
    {
        /// <summary>
        /// Method to create 8 directional and (if any) point markers.
        /// </summary>
        /// <param name="figure"><see cref="Shape"/> to generate markers for.</param>
        /// <param name="viewmodel"><see cref="DrawingViewModel"/> to constrain
        /// the markers.</param>
        public static IEnumerable<MarkerBase> CreateMarkers(Shape figure,
            DrawingViewModel viewmodel)
        {
            yield return new TopLeftMarker(figure, viewmodel);
            yield return new BottomRightMarker(figure, viewmodel);
            yield return new TopRightMarker(figure, viewmodel);
            yield return new BottomLeftMarker(figure, viewmodel);
            yield return new LeftMarker(figure, viewmodel);
            yield return new BottomMarker(figure, viewmodel);
            yield return new RightMarker(figure, viewmodel);
            yield return new TopMarker(figure, viewmodel);

            if (figure is Polygon || figure is Polyline)
            {
                dynamic d = figure;
                for (int i = 0; i < d.Points.Count; i++)
                {
                    yield return new PointMarker(figure, viewmodel, i);
                }
            }
        }
    }
}
