﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Shapes;

namespace VectorAneurysm.Models.DragHandlers
{
    /// <summary>
    /// Base class for mouse move when something is dragged events.
    /// </summary>
    internal abstract class DragHandlerBase
    {
        /// <summary>
        /// States of selected figures: actual as dict's key, before drag as value.
        /// </summary>
        protected readonly IDictionary<Shape, Shape> _selectionStates;

        /// <summary>
        /// Size of the canvas to clamp dragged figures/markers. Width, then height.
        /// </summary>
        protected readonly (double, double) _canvasSize;

        /// <summary>
        /// Indicates whether "even mode" was turned on by user.
        /// </summary>
        protected readonly bool _evenMode;

        /// <summary>
        /// Indicates whether "multi select mode" was turned on by user.
        /// </summary>
        protected readonly bool _multiSelect;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="figures">Actual data about selected figures.</param>
        /// <param name="size">Canvas size.</param>
        /// <param name="evenMode">Even mode state: true is on.</param>
        /// <param name="multiSelect">Multi select state: true is on.</param>
        public DragHandlerBase(IDictionary<Shape, Shape> figures, (double, double) size,
            bool evenMode, bool multiSelect = false)
        {
            _selectionStates = figures ?? throw new ArgumentNullException("figures");
            if (_selectionStates.Keys.Count < 1)
            {
                throw new ArgumentException("figures");
            }
            _canvasSize = size;
            _evenMode = evenMode;
            _multiSelect = multiSelect;
        }

        /// <summary>
        /// Method to handle the drag event.
        /// </summary>
        /// <param name="currentPos">Current position of the cursor relative to
        /// the canvas.</param>
        /// <param name="startPos">Position where drag started relative to the canvas.
        /// </param>
        public abstract void HandleDrag(Point currentPos, Point startPos);
    }
}
