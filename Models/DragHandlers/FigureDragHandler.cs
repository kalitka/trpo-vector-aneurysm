﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Shapes;

using VectorAneurysm.Util;

namespace VectorAneurysm.Models.DragHandlers
{
    /// <summary>
    /// Class that handles drag of figures.
    /// </summary>
    internal class FigureDragHandler : DragHandlerBase
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="figures">Actual data about selected figures.</param>
        /// <param name="size">Canvas size.</param>
        /// <param name="evenMode">Even mode state: true is on.</param>
        /// <param name="multiSelect">Multi select state: true is on.</param>
        public FigureDragHandler(IDictionary<Shape, Shape> figures,
            (double, double) size, bool evenMode, bool multiSelect = false)
            : base(figures, size, evenMode, multiSelect) { }

        /// <summary>
        /// Method to handle the figure drag event by moving the selected figures.
        /// </summary>
        /// <param name="currentPos">Current position of the cursor relative to
        /// the canvas.</param>
        /// <param name="startPos">Position where drag started relative to the canvas.
        /// </param>
        public override void HandleDrag(Point currentPos, Point startPos)
        {
            var dx = currentPos.X - startPos.X;
            var dy = currentPos.Y - startPos.Y;
            if (_evenMode)
            {
                if (Math.Abs(dx) > Math.Abs(dy))
                {
                    dy = 0;
                }
                else
                {
                    dx = 0;
                }
            }
            foreach (var figure in _selectionStates.Keys)
            {
                figure.Move(_selectionStates[figure], dx, dy, _canvasSize);
            }
        }
    }
}
