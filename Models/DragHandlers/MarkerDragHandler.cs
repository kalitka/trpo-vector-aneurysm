﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

using VectorAneurysm.DataStructures;
using VectorAneurysm.Models.Markers;
using VectorAneurysm.Util;

namespace VectorAneurysm.Models.DragHandlers
{
    internal class MarkerDragHandler : DragHandlerBase
    {
        /// <summary>
        /// Marker, dragging of which is handled by this instance.
        /// </summary>
        private readonly MarkerBase _marker;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="marker">Marker that was dragged.</param>
        /// <param name="figures">Actual data about selected figures.</param>
        /// <param name="size">Canvas size.</param>
        /// <param name="evenMode">Even mode state: true is on.</param>
        /// <param name="multiSelect">Multi select state: true is on.</param>
        public MarkerDragHandler(MarkerBase marker, IDictionary<Shape, Shape> figures,
            (double, double) size, bool evenMode, bool multiSelect = false)
            : base(figures, size, evenMode, multiSelect)
        {
            _marker = marker ?? throw new ArgumentNullException("marker");
        }

        /// <summary>
        /// Method to handle the marker drag event.
        /// </summary>
        /// <param name="currentPos">Current position of the cursor relative to
        /// the canvas.</param>
        /// <param name="startPos">Position where drag started relative to the canvas.
        /// </param>
        public override void HandleDrag(Point currentPos, Point startPos)
        {
            var adjustedPosition = ClampUnusedAxis(_marker, currentPos);
            if (_evenMode)
            {
                adjustedPosition = ClampProportionalResize(_marker, adjustedPosition,
                    startPos);
            }
            // this is bad architecture in action: Marker.Center is a read-only Point,
            // but here's an ugly method. Could be done better next time.
            _marker.SetCenter(adjustedPosition.X, adjustedPosition.Y);
            if (_marker is PointMarker pm)
            {
                // why waste another method to literally two lines of code
                dynamic d = _marker.Parent;
                d.Points[pm.Index] = _marker.Center;
            }
            else
            {
                var dx = adjustedPosition.X - startPos.X;
                var dy = adjustedPosition.Y - startPos.Y;

                var draggedParent = _marker.Parent;
                // resize everything relative to bounding rectagle
                if (_multiSelect)
                {
                    var bounding = GetBounding(_selectionStates.Keys);
                    foreach (var figure in _selectionStates.Keys)
                    {
                        var yRatio = _selectionStates[figure].GetHeight() /
                            _selectionStates[draggedParent].GetHeight();
                        var xRatio = _selectionStates[figure].GetWidth() /
                            _selectionStates[draggedParent].GetWidth();

                        figure.ResizeY(_selectionStates[figure],
                            _marker.VerticalAnchor, dy * yRatio,
                            _canvasSize.Item2, bounding);
                        figure.ResizeX(_selectionStates[figure],
                            _marker.HorizontalAnchor, dx * xRatio,
                            _canvasSize.Item1, bounding);
                    }
                }
                // resize just the figure
                else
                {
                    draggedParent.ResizeY(_selectionStates[draggedParent],
                        _marker.VerticalAnchor, dy, _canvasSize.Item2);
                    draggedParent.ResizeX(_selectionStates[draggedParent],
                        _marker.HorizontalAnchor, dx, _canvasSize.Item1);
                }
            }
        }

        /// <summary>
        /// Method that discards changes to position on axis if marker does not control
        /// this axis, e.g. vertical marker is not movable on X axis.
        /// </summary>
        /// <param name="marker">Dragged marker to determine its type.</param>
        /// <param name="position">Unfiltered position.</param>
        /// <returns>Modified position with delta on unused axis discraded.</returns>
        private Point ClampUnusedAxis(MarkerBase marker, Point position)
        {
            var X = marker is PointMarker
                || marker.HorizontalAnchor != HorizontalAnchor.None ?
                position.X : marker.Center.X;
            var Y = marker is PointMarker
                || marker.VerticalAnchor != VerticalAnchor.None ?
                position.Y : marker.Center.Y;
            return new Point(X, Y);
        }

        /// <summary>
        /// Changes lesser of X and Y deltas to match the bigger one in proportion of
        /// original figures's aspect ratio. Only works with corner markers.
        /// </summary>
        /// <param name="marker">Dragged marker to determine its type.</param>
        /// <param name="currentPosition">Mouse position relative to the canvas,</param>
        /// <param name="originalPosition">Drag'n'drop start position relative to
        /// the canvas</param>
        /// <returns>Mouse position that will scale figure(s) proportionately.</returns>
        private Point ClampProportionalResize(MarkerBase marker, Point currentPosition,
            Point originalPosition)
        {
            if (marker.HorizontalAnchor == HorizontalAnchor.None
                || marker.VerticalAnchor == VerticalAnchor.None)
            {
                return currentPosition;
            }
            var originalRatio = _selectionStates[marker.Parent].GetWidth() /
                _selectionStates[marker.Parent].GetHeight();
            var dx = currentPosition.X - originalPosition.X;
            var dy = currentPosition.Y - originalPosition.Y;
            // sign is plus on main diagonal and minus on other diagonal:
            // if we're dragging to top and left (or to right and bottom)
            // dx and dy both > 0 (< 0).
            //They have opposite signs on top-right and bottom-left
            double sign = marker is TopLeftMarker || marker is BottomRightMarker ?
                1d : -1d;
            if (Math.Abs(dx) > Math.Abs(dy))
            {
                // originalRatio is x / y, so here we dividing
                currentPosition.Y = originalPosition.Y + sign * dx / originalRatio;
            }
            else
            {
                // and here we multiplying to get X
                currentPosition.X = originalPosition.X + sign * dy * originalRatio;
            }
            return currentPosition;
        }

        /// <summary>
        /// Returns a bounding rectangle of provided list of shapes.
        /// </summary>
        /// <param name="from">Figures to get bounding.</param>
        /// <returns>Rectangle that encompasses all figures.</returns>
        private Rectangle GetBounding(IEnumerable<Shape> from)
        {
            var minX = from.Min(s => s.GetLeft());
            var maxX = from.Max(s => s.GetRight());
            var minY = from.Min(s => s.GetTop());
            var maxY = from.Max(s => s.GetBottom());
            var bounding = new Rectangle()
            {
                Width = maxX - minX,
                Height = maxY - minY
            };
            Canvas.SetLeft(bounding, minX);
            Canvas.SetTop(bounding, minY);

            return bounding;
        }
    }
}
