﻿using System;
using System.Windows.Input;

namespace VectorAneurysm.Models
{
    /// <summary>
    /// Class to streamline adding commands to app's menu.
    /// See https://msdn.microsoft.com/en-us/magazine/dd419663.aspx
    /// </summary>
    public class RelayCommand : ICommand
    {
        /// <summary>
        /// The action this command performs.
        /// </summary>
        private readonly Action<object> _execute;

        /// <summary>
        /// The predicate that controls when this command can be executed.
        /// </summary>
        private readonly Predicate<object> _canExecute;

        /// <summary>
        /// Constructor with predicate ommited for an always available command.
        /// </summary>
        /// <param name="execute">An action this command will perform.</param>
        public RelayCommand(Action<object> execute) : this(execute, null) { }

        /// <summary>
        /// Main constructor.
        /// </summary>
        /// <param name="execute">An action this command will perform.</param>
        /// <param name="canExecute">Predicate to control if this command
        /// can be executed. If null, it can be always executed.</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            _execute = execute ?? throw new ArgumentNullException("execute");
            _canExecute = canExecute;
        }

        /// <summary>
        /// ICommand implementation.
        /// </summary>
        /// <param name="parameter">Parameter to pass to predicate.</param>
        /// <returns>True if command can be executed.</returns>
        public bool CanExecute(object parameter)
        {
            return _canExecute == null ? true : _canExecute(parameter);
        }

        /// <summary>
        /// ICommand implementation. No idea what's it for.
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        /// <summary>
        /// ICommand implementation.
        /// </summary>
        /// <param name="parameter">Parameter to pass to action.</param>
        public void Execute(object parameter)
        {
            _execute(parameter);
        }
    }
}
