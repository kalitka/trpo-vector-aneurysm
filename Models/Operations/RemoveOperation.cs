﻿using System;
using System.Collections.Generic;
using System.Windows.Shapes;

namespace VectorAneurysm.Models.Operations
{
    /// <summary>
    /// Class that represents operation on removing figures from the canvas.
    /// </summary>
    [Serializable]
    public class RemoveOperation : AddOperation
    {
        // this is basically AddOperation, but methods do the opposite
        // yay code reuse
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="canvas">Canvas that is modified by this operation.</param>
        /// <param name="shapes">Shapes that are removed by this operation.</param>
        public RemoveOperation(CanvasModel canvas, IEnumerable<Shape> shapes)
            : base(canvas, shapes) { }

        /// <summary>
        /// Removes figures from the canvas.
        /// </summary>
        public override void Apply()
        {
            base.Revert();
        }

        /// <summary>
        /// Adds the figures onto the canvas.
        /// </summary>
        public override void Revert()
        {
            base.Apply();
        }
    }
}
