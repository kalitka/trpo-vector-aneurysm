﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Shapes;

using VectorAneurysm.Util;

namespace VectorAneurysm.Models.Operations
{
    /// <summary>
    /// Class that represent resizing operation.
    /// </summary>
    [Serializable]
    public class ResizeOperation : OperationBase
    {
        /// <summary>
        /// No initial application required.
        /// </summary>
        public override bool ApplyOnAdd => false;

        /// <summary>
        /// Holds data about figures: reference to it and scales by X and Y axes as a
        /// <see cref="Point"/> for brevity.
        /// </summary>
        private readonly List<(int, Point)> _states = new List<(int, Point)>();

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="states">Dictonary where value for changed shape is its original
        /// state.</param>
        public ResizeOperation(CanvasModel canvas, Dictionary<Shape, Shape> states)
            : base(canvas)
        {
            if (states == null)
            {
                throw new ArgumentNullException("affected");
            }
            if (states.Keys.Count() < 1)
            {
                throw new ArgumentException("affected");
            }
            foreach (var figure in states.Keys)
            {
                var dx = figure.GetWidth() / states[figure].GetWidth();
                var dy = figure.GetHeight() / states[figure].GetHeight();
                var p = new Point(dx, dy);
                var tuple = (canvas.AllFigures.IndexOf(figure), p);
                _states.Add(tuple);
            }
        }

        /// <summary>
        /// Rescales figures by given multipliers.
        /// </summary>
        public override void Apply()
        {
            foreach (var tuple in _states)
            {
                var figure = _canvas.AllFigures[tuple.Item1];
                figure.RescaleX(tuple.Item2.X);
                figure.RescaleY(tuple.Item2.Y);
            }
        }

        /// <summary>
        /// Rescales figures back.
        /// </summary>
        public override void Revert()
        {
            foreach (var tuple in _states)
            {
                var figure = _canvas.AllFigures[tuple.Item1];
                figure.RescaleX(1 / tuple.Item2.X);
                figure.RescaleY(1 / tuple.Item2.Y);
            }
        }
    }
}
