﻿using System;
using System.Windows;
using System.Windows.Shapes;

namespace VectorAneurysm.Models.Operations
{
    /// <summary>
    /// Represents an operation of mobing a point of polygon or polyline.
    /// </summary>
    [Serializable]
    public class PointMoveOperation : OperationBase
    {
        /// <summary>
        /// This operation has a live preview.
        /// </summary>
        public override bool ApplyOnAdd => false;

        /// <summary>
        /// Change: reference to changed figure, changed point's index,
        /// delta of moving.
        /// </summary>
        private readonly (int, int, Point) _delta;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="states">A tuple of index of figure, affected point's index,
        /// and move delta as <see cref="Point"/>.</param>
        public PointMoveOperation(CanvasModel canvas, (Shape, int, Point) delta)
            : base (canvas)
        {
            _delta = (canvas.AllFigures.IndexOf(delta.Item1), delta.Item2, delta.Item3);
        }

        /// <summary>
        /// Moves affected point.
        /// </summary>
        public override void Apply()
        {
            dynamic s = _canvas.AllFigures[_delta.Item1];
            Point prev = s.Points[_delta.Item2];
            var replacement = new Point()
            {
                X = prev.X + _delta.Item3.X,
                Y = prev.Y + _delta.Item3.Y,
            };
            s.Points[_delta.Item2] = replacement;
        }

        /// <summary>
        /// Moves affected point back.
        /// </summary>
        public override void Revert()
        {
            dynamic s = _canvas.AllFigures[_delta.Item1];
            Point prev = s.Points[_delta.Item2];
            var replacement = new Point()
            {
                X = prev.X - _delta.Item3.X,
                Y = prev.Y - _delta.Item3.Y,
            };
            s.Points[_delta.Item2] = replacement;

        }
    }
}
