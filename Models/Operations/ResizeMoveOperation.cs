﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Shapes;

namespace VectorAneurysm.Models.Operations
{
    /// <summary>
    /// Class than combines <see cref="MoveOperation"/> and <see cref="ResizeOperation"/>
    /// as operation undoable as one. Used in group resizing where figures can also move
    /// to preserve relative placement to each other.
    /// </summary>
    [Serializable]
    public class ResizeMoveOperation : OperationBase
    {
        /// <summary>
        /// This is an operation with a live preview.
        /// </summary>
        public override bool ApplyOnAdd => false;

        /// <summary>
        /// Used instance of <see cref="MoveOperation"/>
        /// </summary>
        private readonly MoveOperation _move;

        /// <summary>
        /// Used instance of <see cref="ResizeOperation"/>
        /// </summary>
        private readonly ResizeOperation _resize;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="states">Dictonary where value for changed shape is its original
        /// state.</param>
        public ResizeMoveOperation(CanvasModel canvas, Dictionary<Shape, Shape> states)
            : base(canvas)
        {
            if (states == null)
            {
                throw new ArgumentNullException("affected");
            }
            if (states.Keys.Count() < 1)
            {
                throw new ArgumentException("affected");
            }
            _move = new MoveOperation(canvas, states);
            _resize = new ResizeOperation(canvas, states);
        }

        /// <summary>
        /// Moves and resized affected figures.
        /// </summary>
        public override void Apply()
        {
            _move.Apply();
            _resize.Apply();
        }

        /// <summary>
        /// Reverts this opeartion by moving and scaling figures back.
        /// </summary>
        public override void Revert()
        {
            _move.Revert();
            _resize.Revert();
        }
    }
}
