﻿using System;
using System.Runtime.Serialization;
using System.Windows.Media;

namespace VectorAneurysm.Models.Operations
{
    /// <summary>
    /// Basic interface for all undoable and redoable operations.
    /// </summary>
    [Serializable]
    public abstract class OperationBase
    {
        /// <summary>
        /// Applies the operation.
        /// </summary>
        public abstract void Apply();

        /// <summary>
        /// Rolls back the operation. Should leave everything as if <see cref="Apply"/>
        /// call never happened.
        /// </summary>
        public abstract void Revert();

        /// <summary>
        /// Indicates whether this operation should be applied when first added
        /// to managing class.
        /// </summary>
        public abstract bool ApplyOnAdd { get; }

        /// <summary>
        /// <see cref="CanvasModel"/> this operation affects;
        /// </summary>
        [NonSerialized]
        protected CanvasModel _canvas;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="canvas"><see cref="CanvasModel"/> to use.</param>
        public OperationBase(CanvasModel canvas)
        {
            _canvas = canvas ?? throw new ArgumentNullException("canvas");
        }

        // this kind of methods should usually be private, as analyzer suggests
        // but none of the child classes needs to add something else to this method
        // so it's inherited anyway
        /// <summary>
        /// Method that fires up on deserializing of this instance. Updates
        /// <see cref="_canvas"/> to point ot actual <see cref="CanvasModel"/> used
        /// in app.
        /// </summary>
        /// <param name="ctx">Context to get reference to new canvas from.</param>
        [OnDeserializing]
        protected void OnDeserializing(StreamingContext ctx)
        {
            _canvas = ctx.Context as CanvasModel ??
                throw new ArgumentException(
                    "Unable to locate canvas in StreamingContext!");
        }

        /// <summary>
        /// Methon to convert <see cref="Color"/> that lacks a [Serialized] attribute
        /// to array of bytes which is serialization-friendlier.
        /// </summary>
        /// <param name="c">Color to convert.</param>
        /// <returns>Array of four bytes: A, R, G, B respectively.</returns>
        protected static byte[] ColorToArray(Color c)
        {
            return new byte[4] { c.A, c.R, c.G, c.B };
        }

        /// <summary>
        /// Method to convert serialization-friendly array of bytes back to
        /// <see cref="Color"/> used with figures.
        /// </summary>
        /// <param name="bytes">Array of bytes containing ARGB values. Exception
        /// is thrown if array's length is not four.</param>
        /// <returns></returns>
        protected static Color ArrayToColor(byte[] bytes)
        {
            if (bytes.Length != 4)
            {
                throw new ArgumentException("Color is constructed from four bytes");
            }
            return Color.FromArgb(bytes[0], bytes[1], bytes[2], bytes[3]);
        }
    }
}
