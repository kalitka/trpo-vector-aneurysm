﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Shapes;

using VectorAneurysm.Util;

namespace VectorAneurysm.Models.Operations
{
    /// <summary>
    /// Class that represents a figure move operations. We don't do any canvas
    /// restraining here because the fact figures can't be moved outside of canvas
    /// is enforced elsewhere.
    /// </summary>
    [Serializable]
    public class MoveOperation : OperationBase
    {
        /// <summary>
        /// Moving is previewed live, no initial actions required.
        /// </summary>
        public override bool ApplyOnAdd => false;

        /// <summary>
        /// Holds data about figures: index of it and deltas by X and Y axes as a
        /// <see cref="Point"/> for brevity.
        /// </summary>
        private readonly List<(int, Point)> _states = new List<(int, Point)>();

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="states">Dictonary where value for changed shape is its original
        /// state.</param>
        public MoveOperation(CanvasModel canvas, Dictionary<Shape, Shape> states)
            : base(canvas)
        {
            if (states == null)
            {
                throw new ArgumentNullException("states");
            }
            if (states.Keys.Count() < 1)
            {
                throw new ArgumentException("states");
            }
            foreach (var figure in states.Keys)
            {
                var dx = figure.GetLeft() - states[figure].GetLeft();
                var dy = figure.GetTop() - states[figure].GetTop();
                var p = new Point(dx, dy);
                var tuple = (canvas.AllFigures.IndexOf(figure), p);
                _states.Add(tuple);
            }
        }

        /// <summary>
        /// Moves affected figures by specified deltas.
        /// </summary>
        public override void Apply()
        {
            foreach (var tuple in _states)
            {
                var figure = _canvas.AllFigures[tuple.Item1];
                figure.SetLeft(figure.GetLeft() + tuple.Item2.X);
                figure.SetTop(figure.GetTop() + tuple.Item2.Y);
            }
        }

        /// <summary>
        /// Moves affected figures by reverse of specified deltas.
        /// </summary>
        public override void Revert()
        {
            foreach (var tuple in _states)
            {
                var figure = _canvas.AllFigures[tuple.Item1];
                figure.SetLeft(figure.GetLeft() - tuple.Item2.X);
                figure.SetTop(figure.GetTop() - tuple.Item2.Y);
            }
        }
    }
}
