﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using System.Windows.Shapes;

namespace VectorAneurysm.Models.Operations
{
    /// <summary>
    /// Represents a change of fill color of a figure.
    /// </summary>
    [Serializable]
    public class FillColorChangedOperation : OperationBase
    {
        /// <summary>
        /// This operation has a live preview so no <see cref="Apply"/> call
        /// straight away.
        /// </summary>
        public override bool ApplyOnAdd => false;

        /// <summary>
        /// List of changes: tuples of indices of affected figure in global list,
        /// old fill color as serialization-friendly array of bytes,
        /// new fill color as serialization-friendly array of bytes.
        /// </summary>
        private readonly List<(int, byte[], byte[])> _states
            = new List<(int, byte[], byte[])>();

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="states">Dictonary where value for changed shape is its original
        /// state.</param>
        public FillColorChangedOperation(CanvasModel canvas,
            Dictionary<Shape, Shape> states) : base(canvas)
        {
            if (states == null)
            {
                throw new ArgumentNullException("states");
            }
            if (!states.Any())
            {
                throw new ArgumentException("states");
            }
            foreach (var figure in states.Keys)
            {
                var outTuple = (canvas.AllFigures.IndexOf(figure),
                    ColorToArray((states[figure].Fill as SolidColorBrush).Color),
                    ColorToArray((figure.Fill as SolidColorBrush).Color));
                _states.Add(outTuple);
            }
        }

        /// <summary>
        /// Sets fill color to new value.
        /// </summary>
        public override void Apply()
        {
            foreach (var tuple in _states)
            {
                (_canvas.AllFigures[tuple.Item1].Fill as SolidColorBrush).Color
                    = ArrayToColor(tuple.Item3);
            }
        }

        /// <summary>
        /// Reverts fill color back to its old value.
        /// </summary>
        public override void Revert()
        {
            foreach (var tuple in _states)
            {
                (_canvas.AllFigures[tuple.Item1].Fill as SolidColorBrush).Color
                    = ArrayToColor(tuple.Item2);
            }
        }
    }
}
