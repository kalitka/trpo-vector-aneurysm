﻿using System;
using System.Windows;

namespace VectorAneurysm.Models.Operations
{
    /// <summary>
    /// Class that represents undoable canvas resize action.
    /// </summary>
    [Serializable]
    public class CanvasResizeOperation : OperationBase
    {
        /// <summary>
        /// This operation should be applied on first addition.
        /// </summary>
        public override bool ApplyOnAdd => true;

        /// <summary>
        /// Changes on both axes as a single point for brevity.
        /// </summary>
        private readonly Point _sizeDelta;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parent"><see cref="CanvasModel"/> instance to affect.
        /// </param>
        /// <param name="delta">Changes of size associated with this opeartion.</param>
        public CanvasResizeOperation(CanvasModel canvas, Point delta) : base(canvas)
        {
            _sizeDelta = delta;
        }

        /// <summary>
        /// Resizes canvas by specified deltas.
        /// </summary>
        public override void Apply()
        {
            _canvas.Width += (int)_sizeDelta.X;
            _canvas.Height += (int)_sizeDelta.Y;
        }

        /// <summary>
        /// Reverts this resize.
        /// </summary>
        public override void Revert()
        {
            _canvas.Width -= (int)_sizeDelta.X;
            _canvas.Height -= (int)_sizeDelta.Y;
        }
    }
}
