﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using System.Windows.Shapes;

namespace VectorAneurysm.Models.Operations
{
    /// <summary>
    /// Represents a change of stroke color of a figure.
    /// </summary>
    [Serializable]
    public class StrokeColorChangedOperation : OperationBase
    {
        /// <summary>
        /// This operation has a live preview so no <see cref="Apply"/> call
        /// straight away.
        /// </summary>
        public override bool ApplyOnAdd => false;

        /// <summary>
        /// List of changes: tuples of indices of affected figures, old stroke color as
        /// byte array for easier serialization, new stroke color also as byte array.
        /// </summary>
        private readonly List<(int, byte[], byte[])> _states
            = new List<(int, byte[], byte[])>();

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="states">Dictonary where value for changed shape is its original
        /// state.</param>
        public StrokeColorChangedOperation(CanvasModel canvas,
            Dictionary<Shape, Shape> states) : base(canvas)
        {
            if (states == null)
            {
                throw new ArgumentNullException("states");
            }
            if (!states.Any())
            {
                throw new ArgumentException("states");
            }
            foreach (var figure in states.Keys)
            {
                var outTuple = (canvas.AllFigures.IndexOf(figure),
                    ColorToArray((states[figure].Stroke as SolidColorBrush).Color),
                    ColorToArray((figure.Stroke as SolidColorBrush).Color));
                _states.Add(outTuple);
            }
        }

        /// <summary>
        /// Sets stroke color to new value.
        /// </summary>
        public override void Apply()
        {
            foreach (var tuple in _states)
            {
                (_canvas.AllFigures[tuple.Item1].Stroke as SolidColorBrush).Color
                    = ArrayToColor(tuple.Item3);
            }
        }

        /// <summary>
        /// Reverts stroke color back to its old value.
        /// </summary>
        public override void Revert()
        {
            foreach (var tuple in _states)
            {
                (_canvas.AllFigures[tuple.Item1].Stroke as SolidColorBrush).Color
                    = ArrayToColor(tuple.Item2);
            }
        }
    }
}