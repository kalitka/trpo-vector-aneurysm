﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Shapes;

namespace VectorAneurysm.Models.Operations
{
    /// <summary>
    /// Class that represents one operation of adding figures to canvas.
    /// </summary>
    [Serializable]
    public class AddOperation : OperationBase
    {
        /// <summary>
        /// Both adding and removal should be applied.
        /// </summary>
        public override bool ApplyOnAdd => true;

        /// <summary>
        /// Holds list of indices of affected shapes in global figures list.
        /// </summary>
        private readonly HashSet<int> _shapes = new HashSet<int>();

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="canvas">Canvas that is modified by this operation.</param>
        /// <param name="shapes">Shapes that are added by this operation.</param>
        public AddOperation(CanvasModel canvas, IEnumerable<Shape> shapes)
            : base(canvas)
        {
            if (shapes == null)
            {
                throw new ArgumentNullException("shapes");
            }
            else if (!shapes.Any())
            {
                throw new ArgumentException("At least one shape must be passed");
            }
            foreach (var s in shapes)
            {
                if (!_canvas.AllFigures.Contains(s))
                {
                    _canvas.AllFigures.Add(s);
                }
                _shapes.Add(_canvas.AllFigures.IndexOf(s));
            }
        }

        /// <summary>
        /// Adds the figures onto the canvas.
        /// </summary>
        public override void Apply()
        {
            foreach (var s in _shapes)
            {
                _canvas.Figures.Add(_canvas.AllFigures[s]);
            }
        }

        /// <summary>
        /// Removes figures from the canvas.
        /// </summary>
        public override void Revert()
        {
            // not sure if these references will work
            // if not, we have Shape.Tag to introduce some kind of ID
            foreach (var s in _shapes)
            {
                var figure = _canvas.AllFigures[s];
                if (_canvas.Figures.Contains(figure))
                {
                    _canvas.Figures.Remove(figure);
                }
                else
                {
                    throw new ApplicationException(
                        "things went wrong, we'll see about that");
                }
            }
        }
    }
}
