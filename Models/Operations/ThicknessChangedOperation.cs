﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Shapes;

namespace VectorAneurysm.Models.Operations
{
    /// <summary>
    /// Class that represents an undoable stroke thickness change operation.
    /// </summary>
    [Serializable]
    public class ThicknessChangedOperation : OperationBase
    {
        /// <summary>
        /// This operation has a live preview so no <see cref="Apply"/> call
        /// straight away.
        /// </summary>
        public override bool ApplyOnAdd => false;

        /// <summary>
        /// List of changes: tuples of index of affected figure, old thickness value,
        /// new thickness value.
        /// </summary>
        private readonly List<(int, double, double)> _states
            = new List<(int, double, double)>();

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="states">Dictonary where value for changed shape is its original
        /// state.</param>
        public ThicknessChangedOperation(CanvasModel canvas,
            Dictionary<Shape, Shape> states) : base(canvas)
        {
            if (states == null)
            {
                throw new ArgumentNullException("states");
            }
            if (!states.Any())
            {
                throw new ArgumentException("states");
            }
            foreach (var figure in states.Keys)
            {
                var tuple = (canvas.AllFigures.IndexOf(figure),
                    states[figure].StrokeThickness, figure.StrokeThickness);
                _states.Add(tuple);
            }
        }

        /// <summary>
        /// Sets thickness to new value.
        /// </summary>
        public override void Apply()
        {
            foreach (var tuple in _states)
            {
                _canvas.AllFigures[tuple.Item1].StrokeThickness = tuple.Item3;
            }
        }

        /// <summary>
        /// Reverts thickness back to its old value.
        /// </summary>
        public override void Revert()
        {
            foreach (var tuple in _states)
            {
                _canvas.AllFigures[tuple.Item1].StrokeThickness = tuple.Item2;
            }
        }
    }
}
