﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using System.Windows.Shapes;

namespace VectorAneurysm.Models.Operations
{
    /// <summary>
    /// Class that represents an undoable stroke type change operation.
    /// </summary>
    [Serializable]
    public class StrokeTypeChangedOperation : OperationBase
    {
        /// <summary>
        /// This operation has a live preview so no <see cref="Apply"/> call
        /// straight away.
        /// </summary>
        public override bool ApplyOnAdd => false;

        /// <summary>
        /// List of changes: tuples of index of affected figure, old stroke type as a
        /// list of doubles rather than DoubleCollection for serialization, new stroke
        /// type also as list of doubles.
        /// </summary>
        private readonly List<(int, List<double>, List<double>)>
            _states = new List<(int, List<double>, List<double>)>();

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="states">Dictonary where value for changed shape is its original
        /// state.</param>
        public StrokeTypeChangedOperation(CanvasModel canvas, 
            Dictionary<Shape, Shape> states) : base(canvas)
        {
            if (states == null)
            {
                throw new ArgumentNullException("states");
            }
            if (!states.Any())
            {
                throw new ArgumentException("states");
            }
            foreach (var inTuple in states)
                foreach (var figure in states.Keys)
                {
                    var tuple = (canvas.AllFigures.IndexOf(figure),
                        states[figure].StrokeDashArray?.CloneCurrentValue().ToList(),
                        figure.StrokeDashArray?.CloneCurrentValue().ToList());
                    _states.Add(tuple);
                }
        }

        /// <summary>
        /// Sets stroke type to new value.
        /// </summary>
        public override void Apply()
        {
            foreach (var tuple in _states)
            {
                _canvas.AllFigures[tuple.Item1].StrokeDashArray
                    = tuple.Item3 == null ?
                    null : new DoubleCollection(tuple.Item3);
            }
        }

        /// <summary>
        /// Reverts stroke type back to its old value.
        /// </summary>
        public override void Revert()
        {
            foreach (var tuple in _states)
            {
                _canvas.AllFigures[tuple.Item1].StrokeDashArray
                    = tuple.Item2 == null ?
                    null : new DoubleCollection(tuple.Item2);
            }
        }
    }
}
