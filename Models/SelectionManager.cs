﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

using VectorAneurysm.DataStructures;
using VectorAneurysm.Models.DragHandlers;
using VectorAneurysm.Models.Markers;
using VectorAneurysm.Util;
using VectorAneurysm.ViewModels;

namespace VectorAneurysm.Models
{
    /// <summary>
    /// Class that makes selecting figures possible.
    /// </summary>
    internal class SelectionManager
    {
        /// <summary>
        /// Indicates whether newly clicked figure will be added to existing selection
        /// or will replace it alltogether.
        /// </summary>
        public bool MultiSelect { get; set; } = false;

        /// <summary>
        /// Indicates whether figures are dragged only along one of the axes,
        /// and whether corner markers scale proportionately.
        /// </summary>
        public bool EvenMode { get; set; } = false;

        /// <summary>
        /// Collection to hold selected figures in.
        /// </summary>
        public ObservableCollection<Shape> Selection { get; }
            = new ObservableCollection<Shape>();

        /// <summary>
        /// Instance of <see cref="ShapeDrawer"/> to draw selection rectangle.
        /// Yay code reuse.
        /// </summary>
        public ShapeDrawer SelectionDrawer { get; }

        /// <summary>
        /// Shape that holds selection rectangle's parameters.
        /// </summary>
        private readonly Shape _selectionPalette;

        /// <summary>
        /// Reference to the viewmodel used throughout the app.
        /// </summary>
        private readonly DrawingViewModel _parent;

        /// <summary>
        /// Used to indicate whether there is a figure moving, and if yes, serves as
        /// marker to where move to.
        /// </summary>
        private Point? _dragNDropKey;

        /// <summary>
        /// Reference to marker being dragged, if any.
        /// </summary>
        private MarkerBase _draggedMarker = null;

        /// <summary>
        /// Collection of <see cref="MarkerBase.Marker"/> to pass to viewmodel to display
        /// on canvas.
        /// </summary>
        public ObservableCollection<Shape> Markers { get; }
            = new ObservableCollection<Shape>();

        /// <summary>
        /// Collection of full-blown <see cref="MarkerBase"/> objects.
        /// </summary>
        private readonly ObservableCollection<MarkerBase> _markers
            = new ObservableCollection<MarkerBase>();

        /// <summary>
        /// Dictionary that holds original data of figures that is used
        /// to change them correctly and create undoable operations.
        /// </summary>
        private readonly Dictionary<Shape, Shape> _originals
            = new Dictionary<Shape, Shape>();

        /// <summary>
        /// Used to keep track of passed time.
        /// </summary>
        private Stopwatch _watch = Stopwatch.StartNew();

        /// <summary>
        /// Delay (in ms) after which palette changes are applied, if there were no
        /// further changes.
        /// </summary>
        private const int _delayBeforeApplying = 225;

        /// <summary>
        /// Holds timestamps at which last change of several palette-related subtypes
        /// occured. Change is only fixed if no further changes were made in a brief
        /// period after.
        /// </summary>
        private readonly Dictionary<ChangeOperationType, long> _lastTimestamps
            = new Dictionary<ChangeOperationType, long>();

        /// <summary>
        /// Indicates whether pallete changes are from newly selected figure
        /// and therefore shouldn't be counted in undo/redo shenanigans.
        /// </summary>
        private bool _palleteUpdating;

        /// <summary>
        /// Indicates something is being dragged on the canvas.
        /// </summary>
        public bool Dragging => _dragNDropKey != null || _draggedMarker != null;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parent">Reference to hosting <see cref="DrawingViewModel"/>
        /// </param>
        public SelectionManager(DrawingViewModel parent)
        {
            _parent = parent ?? throw new ArgumentNullException("parent");
            _selectionPalette = new Rectangle
            {
                // i love orange color
                Fill = new SolidColorBrush(Color.FromArgb(40, 255, 172, 142)),
                Stroke = new SolidColorBrush(Color.FromRgb(0, 0, 0)),
                StrokeThickness = 2,
                StrokeDashArray
                = new DoubleCollection(new List<double>() { 3d, 1d })
            };

            SelectionDrawer = new ShapeDrawer(_parent, _selectionPalette);

            // some kind of wpf magic so we can listen to changes without binding
            // this is needed for whatever reason.
            DependencyPropertyDescriptor.FromProperty(Shape.StrokeThicknessProperty,
                typeof(Shape))
                .AddValueChanged(_parent.Palette, OnPaletteStrokeThicknessChanged);
            DependencyPropertyDescriptor.FromProperty(Shape.FillProperty, typeof(Shape))
                .AddValueChanged(_parent.Palette, OnPaletteFillColorChanged);
            DependencyPropertyDescriptor.FromProperty(Shape.StrokeDashArrayProperty,
                typeof(Shape))
                .AddValueChanged(_parent.Palette, OnPaletteStrokeTypeChanged);
            DependencyPropertyDescriptor.FromProperty(Shape.StrokeProperty,
                typeof(Shape))
                .AddValueChanged(_parent.Palette, OnPaletteStrokeColorChanged);

            Selection.CollectionChanged += OnSelectionChanged;
            _markers.CollectionChanged += OnMarkersChanged;
            _parent.PropertyChanged += (s, e) => InvalidateMarkers();

        }

        /// <summary>
        /// Event handler that keeps <see cref="Markers"/> synced with
        /// <see cref="_markers"/> content.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Event arguments, including added or removed markers.</param>
        private void OnMarkersChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (MarkerBase mbase in e.NewItems)
                    {
                        // will break on over 9000 figures, but why would anyone ever
                        // try that?
                        // i'm bad at computer programming anyway
                        // pepeHands
                        Canvas.SetZIndex(mbase.Marker, 9000);
                        Markers.Add(mbase.Marker);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (MarkerBase mbase in e.OldItems)
                    {
                        Markers.Remove(mbase.Marker);
                    }
                    break;
            }
        }

        /// <summary>
        /// Event handler that adds and removes markers for figures as they are added
        /// and removed from <see cref="Selection"/>.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Event arguments, including added or removed figures.</param>
        private void OnSelectionChanged(object sender,
            NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (Shape figure in e.NewItems)
                    {
                        foreach (var marker in MarkerFactory.CreateMarkers(figure,
                            _parent))
                        {
                            _markers.Add(marker);
                        }
                        _originals[figure] = figure.Clone();
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (Shape figure in e.OldItems)
                    {
                        _originals.Remove(figure);
                        // ToList call prevent it from being modified by Remove and thus
                        // throwing an InvalidOperationException
                        var toRemove = _markers.Where(m => m.Parent == figure).ToList();
                        foreach (var f in toRemove)
                        {
                            _markers.Remove(f);
                        }
                    }
                    if (_dragNDropKey.HasValue)
                    {
                        _dragNDropKey = null;
                    }
                    break;
            }
        }

        /// <summary>
        /// Event handler that removes from selection figures removed from the canvas
        /// so we don't have to do it ourselves every time.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Event arguments including list of removed figures.</param>
        public void OnCanvasFiguresChanged(object sender,
            NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (Shape figure in e.OldItems)
                {
                    if (Selection.Contains(figure))
                    {
                        Selection.Remove(figure);
                    }
                }
            }
        }


        /// <summary>
        /// Destructor that removes magic event handlers.
        /// </summary>
        ~SelectionManager()
        {
            // internets say we should remove these explicitly, or memory will leak
            DependencyPropertyDescriptor.FromProperty(Shape.StrokeThicknessProperty,
                typeof(Shape))
                .RemoveValueChanged(_parent.Palette, OnPaletteStrokeThicknessChanged);
            DependencyPropertyDescriptor.FromProperty(Shape.FillProperty, typeof(Shape))
                .RemoveValueChanged(_parent.Palette, OnPaletteFillColorChanged);
            DependencyPropertyDescriptor.FromProperty(Shape.StrokeDashArrayProperty,
                typeof(Shape))
                .RemoveValueChanged(_parent.Palette, OnPaletteStrokeTypeChanged);
            DependencyPropertyDescriptor.FromProperty(Shape.StrokeProperty,
                typeof(Shape))
                .RemoveValueChanged(_parent.Palette, OnPaletteStrokeColorChanged);
        }

        /// <summary>
        /// Event sender that handles palette's <see cref="Shape.StrokeThickness"/>
        /// changes, mirrors it to selected figures.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Generic event args so this counts as event handler,
        /// unused.</param>
        private void OnPaletteStrokeThicknessChanged(object sender, EventArgs e)
        {
            if (Selection.Any() && !_palleteUpdating)
            {
                foreach (var figure in Selection)
                {
                    figure.StrokeThickness = _parent.Palette.StrokeThickness;
                    // but still, user can create fully transparent figures,
                    // what's the point of this
                    if (figure is Polyline && figure.StrokeThickness == 0)
                    {
                        figure.StrokeThickness = 1;
                    }
                }
                _lastTimestamps[ChangeOperationType.ThicknessChanged]
                    = _watch.ElapsedMilliseconds;
                // the magic to continue execution, but after a slight delay
                // come back and check for further changes: if there's none,
                // fix the change and make it undoable
                Task.Delay(_delayBeforeApplying).GetAwaiter().OnCompleted(() =>
                    CheckAfterDelay(ChangeOperationType.ThicknessChanged));
            }
        }

        /// <summary>
        /// Event sender that handles palette's <see cref="Shape.Fill"/>
        /// changes, mirrors it to selected figures (but not to polylines).
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Generic event args so this counts as event handler,
        /// unused.</param>
        private void OnPaletteFillColorChanged(object sender, EventArgs e)
        {
            if (Selection.Any() && !_palleteUpdating)
            {
                var color = (_parent.Palette.Fill as SolidColorBrush).Color;
                foreach (var figure in Selection)
                {
                    if (!(figure is Polyline))
                    {
                        (figure.Fill as SolidColorBrush).Color = color;
                    }
                }
                _lastTimestamps[ChangeOperationType.FillChanged]
                    = _watch.ElapsedMilliseconds;
                Task.Delay(_delayBeforeApplying).GetAwaiter().OnCompleted(() =>
                    CheckAfterDelay(ChangeOperationType.FillChanged));
            }
        }

        /// <summary>
        /// Event sender that handles palette's <see cref="Shape.StrokeDashArray"/>
        /// changes, mirrors it to selected figures.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Generic event args so this counts as event handler,
        /// unused.</param>
        private void OnPaletteStrokeTypeChanged(object sender, EventArgs e)
        {
            if (Selection.Any() && !_palleteUpdating)
            {
                var clonedDashArray
                    = _parent.Palette.StrokeDashArray?.CloneCurrentValue();
                foreach (var figure in Selection)
                {
                    figure.StrokeDashArray = clonedDashArray;
                }
                _lastTimestamps[ChangeOperationType.DashStyleChanged]
                    = _watch.ElapsedMilliseconds;
                Task.Delay(_delayBeforeApplying).GetAwaiter().OnCompleted(() =>
                    CheckAfterDelay(ChangeOperationType.DashStyleChanged));
            }
        }

        /// <summary>
        /// Event sender that handles palette's <see cref="Shape.Stroke"/>
        /// changes, mirrors it to selected figures.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Generic event args so this counts as event handler,
        /// unused.</param>
        private void OnPaletteStrokeColorChanged(object sender, EventArgs e)
        {
            if (Selection.Any() && !_palleteUpdating)
            {
                var color = (_parent.Palette.Stroke as SolidColorBrush).Color;
                foreach (var figure in Selection)
                {
                    (figure.Stroke as SolidColorBrush).Color = color;
                }
                _lastTimestamps[ChangeOperationType.StrokeChanged]
                    = _watch.ElapsedMilliseconds;
                Task.Delay(_delayBeforeApplying).GetAwaiter().OnCompleted(() =>
                    CheckAfterDelay(ChangeOperationType.StrokeChanged));
            }
        }

        /// <summary>
        /// Updates palette to match the provided <see cref="Shape"/>.
        /// </summary>
        /// <param name="from">Shape to get paramenters from. If polyline,
        /// transparent fill is ignored.</param>
        private void UpdatePalette(Shape from)
        {
            _palleteUpdating = true;
            _parent.Palette.StrokeThickness = from.StrokeThickness;
            _parent.Palette.StrokeDashArray = from.StrokeDashArray?.CloneCurrentValue();
            _parent.Palette.Stroke = from.Stroke.CloneCurrentValue();
            if (!(from is Polyline))
            {
                _parent.Palette.Fill = from.Fill.CloneCurrentValue();
            }
            _palleteUpdating = false;
        }

        /// <summary>
        /// Method that handles left mouse up: click on empty space clears the selection,
        /// click on Shape adds it to selection, if something was dragged, update it.
        /// </summary>
        /// <param name="hitShape">Shape that was clicked on, if any.</param>
        public void HandleLeftClick(Shape hitShape)
        {
            // it's possible to select the selection area without this safeguard
            if (SelectionDrawer.IsDrawing)
            {
                return;
            }
            // Resize marker was dropped, save results
            if (_draggedMarker != null)
            {
                FinalizeMarkerDrag(_draggedMarker);
                _draggedMarker = null;
                _dragNDropKey = null;
            }
            else
            {
                // figure(s) was moved
                if (_dragNDropKey != null)
                {
                    FixChange(ChangeOperationType.Moved);
                    _dragNDropKey = null;
                }
                // figure was just clicked, update the selection
                else if (hitShape != null && !Markers.Contains(hitShape))
                {
                    UpdateSelection(hitShape);
                }
                // empty space was clicked, clear the selection
                else
                {
                    Selection.RemoveByOne();
                }
            }
        }

        /// <summary>
        /// Helper method that finishes up marker dragging depending on its type.
        /// </summary>
        /// <param name="marker">Marker that was dragged.</param>
        private void FinalizeMarkerDrag(MarkerBase marker)
        {
            marker.UpdatePosition();
            // PointMove operations are special snowflakes and are cast differently
            // from other operations
            if (marker is PointMarker pm)
            {
                // always affects one figure, yay optimisation
                _parent.RegisterPointMove(pm.Parent, _originals[pm.Parent],
                    pm.Index);
                _originals[pm.Parent] = pm.Parent.Clone();
            }
            else
            {
                FixChange(ChangeOperationType.ResizedMoved);
            }
        }

        /// <summary>
        /// Helper method that updates the selection with the newly selected shape.
        /// </summary>
        /// <param name="shape">Shape that was selected by user.</param>
        private void UpdateSelection(Shape shape)
        {
            if (!MultiSelect)
            {
                Selection.RemoveByOne();
            }
            Selection.Add(shape);
            if (Selection.Count == 1)
            {
                UpdatePalette(shape);
            }
        }

        /// <summary>
        /// Handles mouse down event, initiates marker drag'n'drop if button was pressed
        /// over the marker.
        /// </summary>
        /// <param name="hitShape">Shape that was under cursor when event occured,
        /// null if none was.</param>
        public void HandleLeftMouseDown(Point position, Shape hitShape)
        {
            // if we hit a marker or a selected figure, we start dragging it
            if (Selection.Contains(hitShape) || Markers.Contains(hitShape))
            {
                _draggedMarker = _markers.FirstOrDefault(m => m.Marker == hitShape);
                _dragNDropKey = position;
            }
            else
            {
                _draggedMarker = null;
                _dragNDropKey = null;
            }
        }

        /// <summary>
        /// Handles right mouse down events to draw selection rectangle.
        /// </summary>
        /// <param name="position">Right click position relative to canvas.</param>
        public void HandleRightClickDown(Point position)
        {
            if (!_dragNDropKey.HasValue && _draggedMarker == null)
            {
                position.X = OddsEnds.Clamp(position.X, 0, _parent.CanvasWidth);
                position.Y = OddsEnds.Clamp(position.Y, 0, _parent.CanvasHeight);
                SelectionDrawer.HandlePrimaryDraw(position, SelectedTool.Rectangle);
            }
        }

        /// <summary>
        /// Handles right mouse up events to finish up drawing of area selection,
        /// only if it was drawed.
        /// </summary>
        /// <param name="position">Right click position relative to canvas.</param>
        public void HandleRightClickUp(Point position)
        {
            if (SelectionDrawer.IsDrawing)
            {
                SelectionDrawer.HandlePrimaryDraw(position, SelectedTool.Rectangle);
            }
        }

        /// <summary>
        /// Handles mouse move, now only for drawing area selection.
        /// </summary>
        /// <param name="position">Current mouse position relative to the canvas.</param>
        public void HandleMouseMove(Point position)
        {
            // is also done in some of the underlying calls, but not in code there
            // i'm bad at computer programming pepeHands
            position.X = OddsEnds.Clamp(position.X, 0, _parent.CanvasWidth);
            position.Y = OddsEnds.Clamp(position.Y, 0, _parent.CanvasHeight);
            // we're drawing selection
            if (SelectionDrawer.IsDrawing)
            {
                SelectionDrawer.HandleMouseMove(position);
            }
            // we're dragging figures
            else if (_dragNDropKey.HasValue)
            {
                DragHandlerBase handler;
                if (_draggedMarker == null)
                {
                    handler = new FigureDragHandler(_originals, _parent.CanvasSize,
                        EvenMode);
                }
                else
                {
                    handler = new MarkerDragHandler(_draggedMarker, _originals,
                        _parent.CanvasSize, EvenMode, MultiSelect);
                }
                handler.HandleDrag(position, _dragNDropKey.Value);
                InvalidateMarkers();
            }
        }
        
        /// <summary>
        /// Updates selection marker. Here to fix selection markers not updating on undo
        /// or redo of move/resize operation with selection active.
        /// </summary>
        /// <param name="markers">Optional list of markers to be updated. If ommited,
        /// all markers present will be updated.</param>
        private void InvalidateMarkers()
        {
            foreach (var m in _markers)
            {
                m.UpdatePosition();
            }
        }

        /// <summary>
        /// Pushes undoable operation to viewmodel and updates <see cref="_originals"/>
        /// to contain freshly changed figures.
        /// </summary>
        /// <param name="operationType">Operation type to report.</param>
        private void FixChange(ChangeOperationType operationType)
        {
            _parent.RegisterChange(operationType, _originals);
            UpdateOriginals();
            // watch resets every three hours, because why would anyone ever use this
            // for more than five minutes, why even use this at first place
            if (_watch.ElapsedMilliseconds > 1000 * 60 * 60 * 3)
            {
                ResetWatch();
            }
        }

        /// <summary>
        /// Helper method to be called on completeion of delay. Checks if there were
        /// any other changes, if not, fixes this change.
        /// </summary>
        /// <param name="opType"></param>
        private void CheckAfterDelay(ChangeOperationType opType)
        {
            if (_watch.ElapsedMilliseconds - _lastTimestamps[opType]
                >= _delayBeforeApplying)
            {
                FixChange(opType);
                _lastTimestamps[opType] = _watch.ElapsedMilliseconds;
            }
        }

        /// <summary>
        /// Resets timer to prevent theoretical overflow. Better safe than sorry.
        /// </summary>
        private void ResetWatch()
        {
            _watch.Stop();
            foreach (var key in _lastTimestamps.Keys)
            {
                _lastTimestamps[key] -= _watch.ElapsedMilliseconds;
            }
            _watch = Stopwatch.StartNew();
        }

        /// <summary>
        /// Breaks any drag'n'drop operations happening: figures and markers are dropped
        /// in place, selection drawing is cancelled.
        /// </summary>
        public void CancelDragging()
        {
            if (_dragNDropKey != null)
            {
                HandleLeftClick(null);
            }
            if (SelectionDrawer.IsDrawing)
            {
                SelectionDrawer.CancelDrawing();
            }
        }

        /// <summary>
        /// Updates original states of figures to be the same as their current states.
        /// Should be called on undo/redo to prevent weird things from happening.
        /// </summary>
        private void UpdateOriginals()
        {
            foreach (var figure in Selection)
            {
                _originals[figure] = figure.Clone();
            }
        }

        /// <summary>
        /// Method that prevents app from freaking out by updating marker positions and
        /// <see cref="_originals"/> dict on undo/redo and resize operations.
        /// </summary>
        public void Cleanup()
        {
            InvalidateMarkers();
            UpdateOriginals();
        }
    }
}
