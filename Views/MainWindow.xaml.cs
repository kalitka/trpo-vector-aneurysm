﻿using System;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Shapes;

using ColorTools;

using VectorAneurysm.ViewModels;

namespace VectorAneurysm.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Used data context casted to concrete type to access fields, methods,
        /// and properties.
        /// </summary>
        private DrawingViewModel _typedContext;

        /// <summary>
        /// Constructor.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            _typedContext = DataContext as DrawingViewModel ??
                throw new ApplicationException("Unable to cast data context.");

            _typedContext.CanvasShapes.CollectionChanged += UpdateCanvasShapes;
        }

        /// <summary>
        /// Opens a specified file.
        /// </summary>
        /// <param name="path">Path to file to open.</param>
        internal void OpenFile(string path)
        {
            _typedContext.PowerLoadFromFile(path);
        }

        /// <summary>
        /// Handles Fill and Stroke radio buttons by switching colorbox's binding
        /// to <see cref="DrawingViewModel.Palette"/>'s Fill or Stroke brush.
        /// </summary>
        /// <param name="sender">Event sender, either of stroke and fill radios.</param>
        /// <param name="e">Event arguments, unused.</param>
        private void FillStrokeToolboxRadio_Checked(object sender, RoutedEventArgs e)
        {
            // if it's null, this is first time init called because one of the radios
            // is set to be checked on startup
            // everything's set up in xaml, just skip this one
            if (colorBox == null)
            {
                return;
            }
            BindingOperations.ClearAllBindings(colorBox);
            var binding = new Binding
            {
                Source = _typedContext
            };
            if (sender == fillToolboxRadio)
            {
                binding.Path = new PropertyPath("Palette.Fill");
            }
            else if (sender == strokeToolboxRadio)
            {
                binding.Path = new PropertyPath("Palette.Stroke");
            }
            else
            {
                throw new ApplicationException("Unrecognized sender.");
            }
            BindingOperations.SetBinding(colorBox,
                ColorControlPanel.SelectedColorBrushProperty, binding);
        }

        // yup, this was pasted back from viewmodel

        /// <summary>
        /// Event handler for ObservableCollections changes, syncs up content
        /// to one collection bound to canvas to display everything.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Event arguments.</param>
        private void UpdateCanvasShapes(object sender,
            NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                // here the underlying collection won't be reset
                // so things will break if it ever resets
                case NotifyCollectionChangedAction.Add:
                    foreach (Shape figure in e.NewItems)
                    {
                        Canvas.Children.Add(figure);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (Shape figure in e.OldItems)
                    {
                        Canvas.Children.Remove(figure);
                    }
                    break;
            }
        }

        /// <summary>
        /// Routes mouse click event to viewmodel depending on pressed button.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Event arguments, including mouse position and button
        /// states.</param>
        private void HandleClick(object sender, MouseButtonEventArgs e)
        {
            Point position = e.GetPosition(Canvas);
            IInputElement hit = Canvas.InputHitTest(position);
            if (e.ChangedButton == MouseButton.Left)
            {
                _typedContext.HandleLeftClick(position, hit as Shape);
            }
            else if (e.ChangedButton == MouseButton.Right)
            {
                _typedContext.HandleRightClick(position);
            }
        }

        /// <summary>
        /// Routes left and right mouse down events to viewmodel.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Event arguments, including mouse position and button
        /// states.</param>
        private void HandleDown(object sender, MouseButtonEventArgs e)
        {
            Point position = e.GetPosition(Canvas);
            IInputElement hit = Canvas.InputHitTest(position);
            if (e.ChangedButton == MouseButton.Left)
            {
                _typedContext.HandleLeftDown(position, hit as Shape);
            }
            else if (e.ChangedButton == MouseButton.Right)
            {
                _typedContext.HandleRightDown(position);
            }
        }

        /// <summary>
        /// Reroutes mouse move to viewmodel.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Event arguments.</param>
        private void ScrollViewer_MouseMove(object sender, MouseEventArgs e)
        {
            Point position = e.GetPosition(Canvas);
            _typedContext.HandleMouseMove(position);
        }

        /// <summary>
        /// Reroutes key down events to viewmodel.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Event arguments.</param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            _typedContext.HandleKeyDown(e.Key);
        }

        /// <summary>
        /// Reroutes key up events to viewmodel.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Event arguments.</param>
        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            _typedContext.HandleKeyUp(e.Key);
        }

        /// <summary>
        /// Reroutes non-selection tools selected event to viewmodel
        /// to reset the selection.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Event arguments, unused.</param>
        private void ToolRadio_Checked(object sender, RoutedEventArgs e)
        {
            _typedContext?.ClearSelection();
            _typedContext?.CancelFigureCommand.Execute(null);
        }

        /// <summary>
        /// Event handler for window's closing. Asks viewmodel if it's ok to quit.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Event arguments, used to cancel this event if needed.</param>
        private void Window_Closing(object sender,
            System.ComponentModel.CancelEventArgs e)
        {
            var canProceed = _typedContext.ShouldExit();
            e.Cancel = !canProceed;
        }

        /// <summary>
        /// Handles cursor leaving drawing area by cancelling dragging of whatever
        /// was dragged, if anything.
        /// </summary>
        /// <param name="sender">Event sender, unused.</param>
        /// <param name="e">Event arguments, including button state.</param>
        private void ScrollViewer_MouseLeave(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed ||
                e.RightButton == MouseButtonState.Pressed)
            {
                _typedContext.CancelAnyDragging();
            }
        }
    }
}
