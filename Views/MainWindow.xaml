﻿<Window x:Class="VectorAneurysm.Views.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:system="clr-namespace:System;assembly=mscorlib"
        xmlns:updown="clr-namespace:NumericUpDownLib;assembly=NumericUpDownLib"
        xmlns:color="clr-namespace:ColorTools;assembly=ColorTools"
        xmlns:converters="clr-namespace:VectorAneurysm.Converters"
        xmlns:viewmodels="clr-namespace:VectorAneurysm.ViewModels"
        xmlns:dataStructures="clr-namespace:VectorAneurysm.DataStructures"
        mc:Ignorable="d"
        Title="{Binding WindowTitle}"
        Height="768" Width="1024"
        MinHeight="768" MinWidth="1024" Icon="../Assets/icon.ico"
        KeyDown="Window_KeyDown" KeyUp="Window_KeyUp"
        Closing="Window_Closing">
    <Window.Resources>
        <system:String x:Key="ToolsRadioGroup">ToolboxRadioGroupKey</system:String>
        <system:String x:Key="FillStrokeRadioGroup">
            ToolboxFillStrokeRadioGroupKey
        </system:String>
        <Thickness x:Key="MarginValue">2 1</Thickness>
        <Style TargetType="RadioButton" BasedOn="{StaticResource {x:Type ToggleButton}}">
            <Setter Property="Margin" Value="2 1"/>
        </Style>
        <BooleanToVisibilityConverter x:Key="BooleanToVisibilityConverter" />
        <converters:ComparisonConverter x:Key="radioToEnumConverter" />
        <converters:InverseBoolConverter x:Key="notConverter" />
    </Window.Resources>
    <Window.DataContext>
        <viewmodels:DrawingViewModel />
    </Window.DataContext>
    <Window.InputBindings>
        <KeyBinding Key="F1" Command="{Binding AboutCommand}"/>
        <KeyBinding Key="Q" Modifiers="Ctrl" Command="{Binding ExitCommand}"/>
        <KeyBinding Key="Delete" Modifiers="Shift" Command="{Binding CanvasClearCommand}"/>
        <KeyBinding Key="R" Modifiers="Ctrl" Command="{Binding CanvasResizeCommand}"/>
        <KeyBinding Key="Return" Command="{Binding FinishFigureCommand}"/>
        <KeyBinding Key="Backspace" Command="{Binding RemoveLastPointCommand}"/>
        <KeyBinding Key="Esc" Command="{Binding CancelFigureCommand}"/>
        <KeyBinding Key="Delete" Command="{Binding DeleteSelectionCommand}"/>
        <KeyBinding Key="X" Modifiers="Ctrl" Command="{Binding CutCommand}"/>
        <KeyBinding Key="V" Modifiers="Ctrl" Command="{Binding PasteCommand}"/>
        <KeyBinding Key="C" Modifiers="Ctrl" Command="{Binding CopyCommand}"/>
        <KeyBinding Key="D" Modifiers="Ctrl" Command="{Binding ClearSelectionCommand}"/>
        <KeyBinding Key="A" Modifiers="Ctrl" Command="{Binding SelectAllCommand}"/>
        <KeyBinding Key="E" Modifiers="Ctrl" Command="{Binding SaveToPngCommand}"/>
        <KeyBinding Key="Z" Modifiers="Ctrl" Command="{Binding UndoCommand}"/>
        <KeyBinding Key="Y" Modifiers="Ctrl" Command="{Binding RedoCommand}"/>
        <KeyBinding Key="N" Modifiers="Ctrl" Command="{Binding NewCommand}"/>
        <KeyBinding Key="S" Modifiers="Ctrl" Command="{Binding SaveCommand}"/>
        <KeyBinding Key="S" Modifiers="Ctrl+Shift" Command="{Binding SaveAsCommand}"/>
        <KeyBinding Key="O" Modifiers="Ctrl" Command="{Binding OpenCommand}"/>
        <KeyBinding Key="Q" Command="{Binding SetToolCommand}"
                    CommandParameter="{x:Static dataStructures:SelectedTool.Selection}"/>
        <KeyBinding Key="W" Command="{Binding SetToolCommand}"
                    CommandParameter="{x:Static dataStructures:SelectedTool.Polygon}"/>
        <KeyBinding Key="E" Command="{Binding SetToolCommand}"
                    CommandParameter="{x:Static dataStructures:SelectedTool.Ellipse}"/>
        <KeyBinding Key="R" Command="{Binding SetToolCommand}"
                    CommandParameter="{x:Static dataStructures:SelectedTool.Rectangle}"/>
        <KeyBinding Key="T" Command="{Binding SetToolCommand}"
                    CommandParameter="{x:Static dataStructures:SelectedTool.Line}"/>
    </Window.InputBindings>
    <DockPanel LastChildFill="True">
        <Menu DockPanel.Dock="Top">
            <MenuItem Header="_File">
                <MenuItem Header="_New" InputGestureText="Ctrl+N"
                          Command="{Binding NewCommand}"/>
                <MenuItem Header="_Open" InputGestureText="Ctrl+O"
                          Command="{Binding OpenCommand}"/>
                <MenuItem Header="_Save" InputGestureText="Ctrl+S"
                          Command="{Binding SaveCommand}"/>
                <MenuItem Header="Save _as..." InputGestureText="Ctrl+Shift+S"
                          Command="{Binding SaveAsCommand}"/>
                <Separator/>
                <MenuItem Header="_Export to PNG..." InputGestureText="Ctrl+E"
                          Command="{Binding SaveToPngCommand}"/>
                <Separator/>
                <MenuItem Name="exitMenuEntry" Header="E_xit"
                          Command="{Binding ExitCommand}" InputGestureText="Ctrl+Q"/>
            </MenuItem>
            <MenuItem Header="_Edit">
                <MenuItem Header="_Selection">
                    <MenuItem Header="Select _All" InputGestureText="Ctrl+A"
                              Command="{Binding SelectAllCommand}"/>
                    <MenuItem Header="_Clear" InputGestureText="Ctrl+D"
                              Command="{Binding ClearSelectionCommand}"/>
                </MenuItem>
                <MenuItem Header="Cu_t" InputGestureText="Ctrl+X"
                          Command="{Binding CutCommand}"/>
                <MenuItem Header="_Copy" InputGestureText="Ctrl+C"
                          Command="{Binding CopyCommand}"/>
                <MenuItem Header="_Paste" InputGestureText="Ctrl+V"
                          Command="{Binding PasteCommand}"/>
                <MenuItem Header="_Remove" InputGestureText="Del"
                          Command="{Binding DeleteSelectionCommand}"/>
                <Separator />
                <MenuItem Header="_Canvas">
                    <MenuItem Header="_Resize..." InputGestureText="Ctrl+R"
                              Command="{Binding CanvasResizeCommand}" />
                    <MenuItem Header="_Clear" InputGestureText="Shift+Del"
                              Command="{Binding CanvasClearCommand}"/>
                </MenuItem>
                <Separator/>
                <MenuItem Header="_Undo" InputGestureText="Ctrl+Z"
                          Command="{Binding UndoCommand}"/>
                <MenuItem Header="_Redo" InputGestureText="Ctrl+Shift+Z"
                          Command="{Binding RedoCommand}"/>
            </MenuItem>
            <MenuItem Header="_About">
                <MenuItem Name="aboutMenuEntry" Header="_About Vector Aneurysm"
                          Command="{Binding AboutCommand}" InputGestureText="F1"/>
            </MenuItem>
        </Menu>
        <!-- Bg color is to prevent panel
            and canvas blending in on small window sizes such as default -->
        <StackPanel DockPanel.Dock="Left" Orientation="Vertical"
                    Background="WhiteSmoke">
            <StackPanel Orientation="Vertical">
                <RadioButton IsChecked="{Binding Path=CurrentTool,
                                Converter={StaticResource radioToEnumConverter},
                                ConverterParameter={x:Static
                                    dataStructures:SelectedTool.Selection}}"
                             GroupName="{StaticResource ToolsRadioGroup}"
                             Checked="ToolRadio_Checked">
                    Selection
                </RadioButton>
                <RadioButton IsChecked="{Binding Path=CurrentTool,
                                Converter={StaticResource radioToEnumConverter},
                                ConverterParameter={x:Static
                                    dataStructures:SelectedTool.Polygon}}"
                             GroupName="{StaticResource ToolsRadioGroup}"
                             Checked="ToolRadio_Checked">
                    Polygon
                </RadioButton>
                <RadioButton IsChecked="{Binding Path=CurrentTool,
                                Converter={StaticResource radioToEnumConverter},
                                ConverterParameter={x:Static
                                    dataStructures:SelectedTool.Ellipse}}"
                             GroupName="{StaticResource ToolsRadioGroup}"
                             Checked="ToolRadio_Checked">
                    Ellipse
                </RadioButton>
                <RadioButton IsChecked="{Binding Path=CurrentTool,
                                Converter={StaticResource radioToEnumConverter},
                                ConverterParameter={x:Static
                                    dataStructures:SelectedTool.Rectangle}}"
                             GroupName="{StaticResource ToolsRadioGroup}"
                             Checked="ToolRadio_Checked">
                    Rectangle
                </RadioButton>
                <RadioButton IsChecked="{Binding Path=CurrentTool,
                                Converter={StaticResource radioToEnumConverter},
                                ConverterParameter={x:Static
                                    dataStructures:SelectedTool.Line}}"
                             GroupName="{StaticResource ToolsRadioGroup}"
                             Checked="ToolRadio_Checked">
                    Line and Polyline
                </RadioButton>
            </StackPanel>
            <Separator />
            <Grid Margin="0 0 0 2">
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="1*" />
                    <ColumnDefinition Width="1*" />
                </Grid.ColumnDefinitions>
                <RadioButton Grid.Column="0" IsChecked="True"
                             GroupName="{StaticResource FillStrokeRadioGroup}"
                             Name="fillToolboxRadio"
                             Checked="FillStrokeToolboxRadio_Checked">
                    Fill
                </RadioButton>
                <RadioButton Grid.Column="1"
                             GroupName="{StaticResource FillStrokeRadioGroup}"
                             Name="strokeToolboxRadio"
                             Checked="FillStrokeToolboxRadio_Checked">
                    Stroke
                </RadioButton>
            </Grid>
            <color:ColorControlPanel x:Name="colorBox" TextBoxBackground="White"
                                     TextForeground="Black" DockAlphaVisibility="Visible"
                                     TextBoxBorder="Black"
                                     SelectedColorBrush="{Binding Path=Palette.Fill}"
                                     Margin="{StaticResource MarginValue}"/>
            <StackPanel Orientation="Vertical"
                        Visibility="{Binding IsChecked, ElementName=strokeToolboxRadio,
                            Converter={StaticResource BooleanToVisibilityConverter}}">
                <Grid>
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="3*" />
                        <ColumnDefinition Width="1*" />
                    </Grid.ColumnDefinitions>
                    <Slider Maximum="20" TickPlacement="Both" TickFrequency="1"
                            Orientation="Horizontal" Grid.Column="0"
                            Margin="{StaticResource MarginValue}"
                            Value="{Binding Path=Palette.StrokeThickness, Mode=TwoWay}"/>
                    <updown:NumericUpDown Grid.Column="1" MinValue="0" MaxValue="20"
                                          StepSize="1" IsReadOnly="False"
                                          DisplayLength="2"
                                          Margin="{StaticResource MarginValue}"
                                          Value="{Binding Path=Palette.StrokeThickness,
                                            Mode=TwoWay}"/>
                </Grid>
                <Grid>
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="1*" />
                        <ColumnDefinition Width="1*" />
                    </Grid.ColumnDefinitions>
                    <Label Grid.Column="0" Margin="{StaticResource MarginValue}"
                           VerticalContentAlignment="Center">
                        Stroke pattern:
                    </Label>
                    <!-- unfortunately, there's no easy way to make combobox always
                        as wide as its widest element. That's definitely possible, but
                        with a horrible code-behind, so not now, at least-->
                    <ComboBox Name="strokeTypeCombobox" Grid.Column="1"
                              DisplayMemberPath="Caption" SelectedValuePath="Pattern"
                              Margin="{StaticResource MarginValue}"
                              VerticalContentAlignment="Center"
                              ItemsSource="{Binding Path=StrokeTypes}"
                              SelectedValue="{Binding Path=Palette.StrokeDashArray}"/>
                </Grid>
            </StackPanel>
        </StackPanel>
        <ScrollViewer HorizontalScrollBarVisibility="Auto"
                      VerticalScrollBarVisibility="Auto" Background="Gray"
                      MouseUp="HandleClick" PreviewMouseDown="HandleDown"
                      MouseMove="ScrollViewer_MouseMove"
                      MouseLeave="ScrollViewer_MouseLeave">
            <Border Padding="48" >
                <Canvas Width="{Binding Path=CanvasWidth}"
                        Height="{Binding Path=CanvasHeight}"
                        Background="White" Name="Canvas" SnapsToDevicePixels="True"/>
            </Border>
        </ScrollViewer>
    </DockPanel>
</Window>