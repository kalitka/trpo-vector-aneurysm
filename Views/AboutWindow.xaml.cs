﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Windows;
using System.Windows.Documents;

namespace VectorAneurysm.Views
{
    /// <summary>
    /// Interaction logic for AboutWindow.xaml
    /// </summary>
    public partial class AboutWindow : Window
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public AboutWindow()
        {
            InitializeComponent();
            // ha-ha, classic
            versionTextBlock.Text = "Version " +
                Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        /// <summary>
        /// Event handler that start processes for links specified
        /// in Hyperlinks's NavigateUri property.
        /// </summary>
        /// <param name="sender">Event sender. Should be a Hyperlink instance.</param>
        /// <param name="e">Event argumens, unused.</param>
        private void Hyperlink_RequestNavigate(object sender,
            System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            // redundant?
            if (!(sender is Hyperlink))
            {
                throw new ApplicationException(
                    "This event handler only works on Hyperlinks!");
            }

            Process.Start((sender as Hyperlink).NavigateUri.AbsoluteUri);
        }
    }
}
