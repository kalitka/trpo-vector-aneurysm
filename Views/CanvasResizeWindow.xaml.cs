﻿using System.Windows;

namespace VectorAneurysm.Views
{
    /// <summary>
    /// Interaction logic for CanvasResizeWindow.xaml
    /// </summary>
    public partial class CanvasResizeWindow : Window
    {
        /// <summary>
        /// Variable to keep track of initial width to recalculate height when
        /// maintaining aspect ratio is on.
        /// </summary>
        private readonly int _initialWidth;

        /// <summary>
        /// Variable to keep track of initial height to recalculate height when
        /// maintaining aspect ratio is on.
        /// </summary>
        private readonly int _initialHeight;

        /// <summary>
        /// Variable that indicates whether the updown's changes are done by code rather
        /// than user to prevent endless loop of updowns correcting each other's value
        /// when aspect ratio is preserved.
        /// </summary>
        private bool _isAutoChanging;

        /// <summary>
        /// Dialog return value. First item is width, second is height.
        /// </summary>
        public (int, int) NewSize
        {
            get
            {
                // yay tuples
                return (widthUpDown.Value, heightUpDown.Value);
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="width">Initial width of the canvas to resize.</param>
        /// <param name="height">Initial height of the canvas to resize.</param>
        public CanvasResizeWindow(int width, int height)
        {
            _initialWidth = width;
            _initialHeight = height;

            InitializeComponent();

            widthUpDown.Value = width;
            heightUpDown.Value = height;
        }

        /// <summary>
        /// Calculates height that together with given width makes rectangle with the
        /// same aspect ratio as in initial canvas size.
        /// </summary>
        /// <param name="width">New canvas width.</param>
        /// <returns>New canvas height.</returns>
        private int CalculateProportionalHeight(int width)
        {
            double oldWidth = _initialWidth;
            return (int)(width / oldWidth * _initialHeight);
        }

        /// <summary>
        /// Calculates width that together with given height makes rectangle with the
        /// same aspect ratio as in initial canvas size.
        /// </summary>
        /// <param name="height">New canvas height.</param>
        /// <returns>New canvas width.</returns>
        private int CalculateProportionalWidth(int height)
        {
            double oldHeight = _initialHeight;
            return (int)(height / oldHeight * _initialWidth);
        }

        /// <summary>
        /// Shorthand method to get whether checkboxes' bool? IsChecked property is true.
        /// </summary>
        /// <returns>True if checked, false on null or not checked.</returns>
        private bool IsMaintainingRatio()
        {
            return aspectRatioCheckbox.IsChecked.IsTrue();
        }

        /// <summary>
        /// Ok button click handler. Closes window with true DialogResult.
        /// </summary>
        /// <param name="sender">Event sender, unused, though should be okButton.</param>
        /// <param name="e">Event arguments, unused.</param>
        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        /// <summary>
        /// Cancel button click handler. Closes window with false DialogResult.
        /// </summary>
        /// <param name="sender">Event sender, unused,
        /// though should be cancelButton.</param>
        /// <param name="e">Event arguments, unused.</param>
        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        /// <summary>
        /// Checkbox check handler. If checked on, changes height control to maintain
        /// canvas' aspect ratio. This behavior is peeked on in paint.net.
        /// </summary>
        /// <param name="sender">Event sender, unused,
        /// though should be aspectRatioCheckbox.</param>
        /// <param name="e">Event arguments, unused.</param>
        private void aspectRatioCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            if (IsMaintainingRatio())
            {
                _isAutoChanging = true;
                heightUpDown.Value = CalculateProportionalHeight(widthUpDown.Value);
                _isAutoChanging = false;
            }
        }

        /// <summary>
        /// Handles width value changes by correting height to maintain aspect ratio
        /// is necessary.
        /// </summary>
        /// <param name="sender">Event sender, unused,
        /// though should be widthUpDown.</param>
        /// <param name="e">Event arguments, unused.</param>
        private void widthUpDown_ValueChanged(object sender,
            RoutedPropertyChangedEventArgs<int> e)
        {
            if (IsMaintainingRatio() && !_isAutoChanging)
            {
                _isAutoChanging = true;
                heightUpDown.Value = CalculateProportionalHeight(widthUpDown.Value);
                _isAutoChanging = false;
            }
        }

        /// <summary>
        /// Handles height value changes by correting width to maintain aspect ratio
        /// is necessary.
        /// </summary>
        /// <param name="sender">Event sender, unused,
        /// though should be widthUpDown.</param>
        /// <param name="e">Event arguments, unused.</param>
        private void heightUpDown_ValueChanged(object sender,
            RoutedPropertyChangedEventArgs<int> e)
        {
            if (IsMaintainingRatio() && !_isAutoChanging)
            {
                _isAutoChanging = true;
                widthUpDown.Value = CalculateProportionalWidth(heightUpDown.Value);
                _isAutoChanging = false;
            }
        }
    }
}
