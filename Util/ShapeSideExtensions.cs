﻿using VectorAneurysm.DataStructures;

namespace VectorAneurysm.Util
{
    /// <summary>
    /// Provides reuseable helper methods for <see cref="ShapeSide"/> enum.
    /// </summary>
    public static class ShapeSideExtensions
    {
        /// <summary>
        /// Helper method to check axis of side provided. IsVertical is this negated,
        /// so no explicit method for it.
        /// </summary>
        /// <param name="side"><see cref="ShapeSide"/> to test.</param>
        /// <returns>True if enum value corresponds either to left or right side,
        /// false otherwise.</returns>
        public static bool IsHorizontal(this ShapeSide side)
        {
            return side == ShapeSide.Left || side == ShapeSide.Right;
        }

        /// <summary>
        /// Helper method to check whether side provided has smaller coordinates that
        /// the opposite on the same axis.
        /// </summary>
        /// <param name="side"><see cref="ShapeSide"/> to test.</param>
        /// <returns>True for left and top sides, false otherwise.</returns>
        public static bool IsLesserCoordinate(this ShapeSide side)
        {
            return side == ShapeSide.Left || side == ShapeSide.Top;
        }
    }
}
