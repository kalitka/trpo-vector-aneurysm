﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VectorAneurysm
{
    /// <summary>
    /// Class that holds some helper methods to small to bother with separate classes.
    /// </summary>
    public static class OddsEnds
    {
        // writing this is faster and easier than googling

        /// <summary>
        /// Extension method for nullable boolean to check whether it's true with one
        /// method call.
        /// </summary>
        /// <param name="b">bool? to test.</param>
        /// <returns>True if b has true value, false on null and false.</returns>
        public static bool IsTrue(this bool? b)
        {
            return b.HasValue && b.Value;
        }

        /// <summary>
        /// Ensures given double lies within given bounds. If it's lower, it's replaced
        /// by lower bound, if greater -- replaced by upper bound.
        /// </summary>
        /// <param name="value">Value to clamp.</param>
        /// <param name="low">Lower bound of interval.</param>
        /// <param name="high">Upper bound of interval.</param>
        /// <returns>Clamped value that is sure to be within given bounds.</returns>
        public static double Clamp(double value, double low, double high)
        {
            return Math.Min(Math.Max(low, value), high);
        }

        /// <summary>
        /// Method to clear the collection by one element at a time to generate
        /// element removal events in observable collections.
        /// </summary>
        /// <typeparam name="TSource">Type of elements inside the enumerable.</typeparam>
        /// <param name="enumearble"><see cref="IList{TSource}"/> to clear.</param>
        public static void RemoveByOne<TSource>(this IList<TSource> enumearble)
        {
            while (enumearble.Any())
            {
                enumearble.RemoveAt(0);
            }
        }
    }
}
