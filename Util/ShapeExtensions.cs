﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Shapes;
using System.Xml;

using VectorAneurysm.DataStructures;

namespace VectorAneurysm.Util
{
    /// <summary>
    /// Helper class that contains all the Shape crutches needed to make this thing work.
    /// </summary>
    public static class ShapeExtensions
    {
        /// <summary>
        /// Deep clones a Shape by serializing it to XAML and back to new instance.
        /// </summary>
        /// <param name="s">Shape to clone.</param>
        /// <returns>Deep copy of <paramref name="s"/>.</returns>
        public static Shape Clone(this Shape s)
        {
            var serialized = XamlWriter.Save(s);
            var sr = new StringReader(serialized);
            var xmlr = XmlReader.Create(sr);
            return (Shape)XamlReader.Load(xmlr);
        }

        /// <summary>
        /// Rescales shape along given axis (any of the two) while preserving original
        /// top-left coordinates.
        /// </summary>
        /// <param name="s">Shape to rescale</param>
        /// <param name="axis">Axis to scale along.</param>
        /// <param name="ratio">Multiplier to original size.</param>
        private static void Rescale(Shape s, Axis axis, double ratio)
        {
            if (s is Polygon || s is Polyline)
            {
                dynamic typed = s;
                var selector = axis == Axis.Horizontal ?
                    (Func<Point, double>)(p => p.X) : (p => p.Y);
                double min = Enumerable.Min(typed.Points, selector);
                for (int i = 0; i < typed.Points.Count; i++)
                {
                    Point current = typed.Points[i];
                    Point replacement = new Point()
                    {
                        X = axis == Axis.Horizontal ?
                            min + ratio * (current.X - min) : current.X,
                        Y = axis == Axis.Horizontal ?
                            current.Y : min + ratio * (current.Y - min)
                    };
                    typed.Points[i] = replacement;
                }
            }
            else
            {
                if (axis == Axis.Horizontal)
                {
                    s.Width *= ratio;
                }
                else
                {
                    s.Height *= ratio;
                }
            }
        }

        /// <summary>
        /// Rescales given <see cref="Shape"/> along horizontal axis by given ratio
        /// while preserving original left coordiante.
        /// </summary>
        /// <param name="s">Shape to rescale.</param>
        /// <param name="ratio">Multiplier for original width.</param>
        public static void RescaleX(this Shape s, double ratio)
            => Rescale(s, Axis.Horizontal, ratio);

        /// <summary>
        /// Rescales given <see cref="Shape"/> along vertical axis by given ratio
        /// while preserving original top coordinate.
        /// </summary>
        /// <param name="s">Shape to rescale.</param>
        /// <param name="ratio">Multiplier for original height.</param>
        public static void RescaleY(this Shape s, double ratio)
            => Rescale(s, Axis.Vertical, ratio);

        /// <summary>
        /// Ultra-mega-compact method to get any coordinate from a shape, because
        /// apparently four similar methods are bad for this.
        /// </summary>
        /// <param name="s">Shape to get value from.</param>
        /// <param name="side">Side of shape to get coordinate.</param>
        /// <returns>Value of given coordinate.</returns>
        private static double GetCoord(Shape s, ShapeSide side)
        {
            if (s is Polygon || s is Polyline)
            {
                dynamic typed = s;
                var selector = side.IsHorizontal() ?
                    (Func<Point, double>)(p => p.X) : (p => p.Y);
                return  side.IsLesserCoordinate() ?
                    Enumerable.Min(typed.Points, selector) :
                    Enumerable.Max(typed.Points, selector);
            }
            else
            {
                // left for X coordinates, top for Y ones
                var baseValue = side.IsHorizontal() ?
                    Canvas.GetLeft(s) : Canvas.GetTop(s);
                // dimension of shape itself if needed
                double addition = 0;
                switch (side)
                {
                    case ShapeSide.Bottom:
                        addition = s.Height;
                        break;
                    case ShapeSide.Right:
                        addition = s.Width;
                        break;
                }
                return baseValue + addition;
            }
        }

        /// <summary>
        /// Replacement of <see cref="Canvas.GetLeft(System.Windows.UIElement)"/>
        /// which doesn't work and returns NaN on polylines and polygons.
        /// </summary>
        /// <param name="s">Shape to get left for.</param>
        /// <returns>Leftmost coordinate of a Shape.</returns>
        public static double GetLeft(this Shape s)
            => GetCoord(s, ShapeSide.Left);

        /// <summary>
        /// Replacement of <see cref="Canvas.GetRight(System.Windows.UIElement)"/>
        /// which doesn't work and returns NaN on polylines and polygons.
        /// </summary>
        /// <param name="s">Shape to get top for.</param>
        /// <returns>Topmost coordinate of a Shape.</returns>
        public static double GetTop(this Shape s)
            => GetCoord(s, ShapeSide.Top);

        /// <summary>
        /// Replacement of <see cref="Canvas.GetRight(System.Windows.UIElement)"/>
        /// which doesn't work and returns NaN.
        /// </summary>
        /// <param name="s">Shape to get right for.</param>
        /// <returns>Rightmost coordinate of a Shape.</returns>
        public static double GetRight(this Shape s)
            => GetCoord(s, ShapeSide.Right);

        /// <summary>
        /// Replacement of <see cref="Canvas.GetBottom(System.Windows.UIElement)"/>
        /// which doesn't work and returns NaN.
        /// </summary>
        /// <param name="s">Shape to get bottom for.</param>
        /// <returns>Bottommest (is this even a word) coordinate of a Shape.</returns>
        public static double GetBottom(this Shape s)
            => GetCoord(s, ShapeSide.Bottom);

        /// <summary>
        /// Ultra-mega-method to set any coordinate of a shape. Still no idea why
        /// slightly modified copypasted methods are bad -- at least they're easier to
        /// understand.
        /// </summary>
        /// <param name="s">Shape to set coordinate.</param>
        /// <param name="side">What side exactly to set.</param>
        /// <param name="value">Value to set.</param>
        private static void SetCoord(Shape s, ShapeSide side, double value)
        {
            if (s is Polygon || s is Polyline)
            {
                var oldValue = GetCoord(s, side);
                dynamic d = s;
                for (int i = 0; i < d.Points.Count; i++)
                {
                    var replacement = new Point
                    {
                        // first argument of ternary here is what happens on horizontal
                        X = side.IsHorizontal() ?
                            value + d.Points[i].X - oldValue : d.Points[i].X,
                        Y = side.IsHorizontal() ?
                            d.Points[i].Y : value + d.Points[i].Y - oldValue
                    };
                    d.Points[i] = replacement;
                }
            }
            else
            {
                var setMethod = side.IsHorizontal() ?
                    (Action<UIElement, double>)Canvas.SetLeft : Canvas.SetTop;
                double displacement = 0;
                // this is why we can't have nice things
                if (!side.IsLesserCoordinate())
                {
                    displacement = side.IsHorizontal() ? s.GetWidth() : s.GetHeight();
                }
                setMethod.Invoke(s, value - displacement);
            }
        }

        /// <summary>
        /// Sets left edge of the <see cref="Shape"/> to desired value.
        /// </summary>
        /// <param name="s">Figure to move.</param>
        /// <param name="left">New leftmost coordinate of the figure.</param>
        public static void SetLeft(this Shape s, double left)
            => SetCoord(s, ShapeSide.Left, left);

        /// <summary>
        /// Sets right edge of the <see cref="Shape"/> to desired value.
        /// </summary>
        /// <param name="s">Figure to move.</param>
        /// <param name="right">New rightmost coordinate of the figure.</param>
        public static void SetRight(this Shape s, double right)
            => SetCoord(s, ShapeSide.Right, right);

        /// <summary>
        /// Sets top edge of the <see cref="Shape"/> to desired value.
        /// </summary>
        /// <param name="s">Figure to move.</param>
        /// <param name="top">New topmost coordinate of the figure.</param>
        public static void SetTop(this Shape s, double top)
            => SetCoord(s, ShapeSide.Top, top);

        /// <summary>
        /// Sets right edge of the <see cref="Shape"/> to desired value.
        /// </summary>
        /// <param name="s">Figure to move.</param>
        /// <param name="bottom">New rightmost coordinate of the figure.</param>
        public static void SetBottom(this Shape s, double bottom)
            => SetCoord(s, ShapeSide.Bottom, bottom);

        /// <summary>
        /// *Reliably* moves the figure relative to its original position
        /// whether it's point based or not while staying within given bounds.
        /// </summary>
        /// <param name="s">Figure to move.</param>
        /// <param name="original">Figure data from before moving even started.</param>
        /// <param name="dx">Amount to move on horizontal axis.</param>
        /// <param name="dy">Amount to move on vertical. axis.</param>
        /// <param name="size">Size of canvas to constrain within.</param>
        public static void Move(this Shape s, Shape original, double dx, double dy,
            (double, double) size)
        {
            var l = OddsEnds.Clamp(original.GetLeft() + dx, 0,
                size.Item1 - s.GetWidth());
            var t = OddsEnds.Clamp(original.GetTop() + dy, 0,
                size.Item2 - s.GetHeight());
            s.SetLeft(l);
            s.SetTop(t);
        }

        /// <summary>
        /// Method to get any of shape's dimensions.
        /// </summary>
        /// <param name="s">Shape to measure.</param>
        /// <param name="axis">Axis to measure along.</param>
        /// <returns>Span of figure by given axis.</returns>
        private static double GetDimension(Shape s, Axis axis)
        {
            var bigger = axis == Axis.Horizontal ? s.GetRight() : s.GetBottom();
            var smaller = axis == Axis.Horizontal ? s.GetLeft() : s.GetTop();
            return bigger - smaller;
        }

        /// <summary>
        /// *Reliably* returns <see cref="Shape"/>'s width.
        /// </summary>
        /// <param name="s">Shape to measure.</param>
        /// <returns>Shapes's width.</returns>
        public static double GetWidth(this Shape s)
            => GetDimension(s, Axis.Horizontal);

        /// <summary>
        /// *Reliably* returns <see cref="Shape"/>'s height.
        /// </summary>
        /// <param name="s">Shape to measure.</param>
        /// <returns>Shapes's height.</returns>
        public static double GetHeight(this Shape s)
            => GetDimension(s, Axis.Vertical);

        /// <summary>
        /// Method to resize a shape. Alicorn magic inside. Do not try to understand.
        /// I repeat: do not try to understand. Really. It's for your own good.
        /// </summary>
        /// <param name="s">Shape to resize.</param>
        /// <param name="original">Shape's original state.</param>
        /// <param name="anchor">Anchor to resize, either <see cref="VerticalAnchor"/> or
        /// <see cref="HorizontalAnchor"/>.</param>
        /// <param name="delta">Amount to resize.</param>
        /// <param name="maxCoord">Maximum allowed coordinate on resize axis.</param>
        /// <param name="multiSelectBounds">Bounding rectangle of all resized figures, or
        /// null if it's a single figure.</param>
        private static void Resize(Shape s, Shape original, Enum anchor, double delta,
            double maxCoord, Shape multiSelectBounds = null)
        {
            // I HAVE NO IDEA WHAT I'M DOING
            bool isHorizontal;
            bool isBiggerCoordAnchor;
            // both validation and determination of axis
            switch (anchor)
            {
                case HorizontalAnchor ha:
                    if (ha == HorizontalAnchor.None)
                    {
                        return;
                    }
                    isHorizontal = true;
                    isBiggerCoordAnchor = ha == HorizontalAnchor.Right;
                    break;
                case VerticalAnchor va:
                    if (va == VerticalAnchor.None)
                    {
                        return;
                    }
                    isHorizontal = false;
                    isBiggerCoordAnchor = va == VerticalAnchor.Bottom;
                    break;
                default:
                    throw new ArgumentException("anchor");
            }
            // if no bounding given, assume bounds are encompassing this figure only
            if (multiSelectBounds == null)
            {
                multiSelectBounds = original;
            }
            // original size
            double q0 = isHorizontal ? original.GetWidth() : original.GetHeight();
            // delta given its sign
            double dq = isBiggerCoordAnchor ? delta : -delta;
            // new size by this dimension
            double q = Math.Min(Math.Max(q0 + dq, 1), maxCoord);
            // coordinate of bounding rect that will not move
            double zeroCoord;
            if (isHorizontal)
            {
                zeroCoord = isBiggerCoordAnchor ?
                    multiSelectBounds.GetLeft() : multiSelectBounds.GetRight();
            }
            else
            {
                zeroCoord = isBiggerCoordAnchor ?
                    multiSelectBounds.GetTop() : multiSelectBounds.GetBottom();
            }
            // now i really have no idea how this works, but it does somehow
            if (s is Polygon || s is Polyline)
            {
                dynamic d = s;
                dynamic o = original;
                for (int i = 0; i < d.Points.Count; i++)
                {
                    var replacement = new Point
                    {
                        // the very essence of dark magic
                        X = isHorizontal ?
                            zeroCoord + q * (o.Points[i].X - zeroCoord) / q0 :
                            d.Points[i].X,
                        Y = isHorizontal ?
                            d.Points[i].Y :
                            zeroCoord + q * (o.Points[i].Y - zeroCoord) / q0
                    };
                    d.Points[i] = replacement;
                }
            }
            else
            {
                if (isHorizontal)
                {
                    s.Width = q;
                }
                else
                {
                    s.Height = q;
                }
            }
            // other coord preservation, whatever that means
            double originalValue;
            Action<Shape, double> setMethod;
            if (isHorizontal)
            {
                if (isBiggerCoordAnchor)
                {
                    originalValue = original.GetLeft();
                    setMethod = SetLeft;
                }
                else
                {
                    originalValue = original.GetRight();
                    setMethod = SetRight;
                }
            }
            else
            {
                if (isBiggerCoordAnchor)
                {
                    originalValue = original.GetTop();
                    setMethod = SetTop;
                }
                else
                {
                    originalValue = original.GetBottom();
                    setMethod = SetBottom;
                }
            }
            double value = zeroCoord + q / q0 * (originalValue - zeroCoord);
            value = OddsEnds.Clamp(value, 0, maxCoord);
            setMethod.Invoke(s, value);
            // additional safegurads piled up
            if (isHorizontal)
            {
                if (s.GetLeft() < 0)
                {
                    s.SetLeft(0);
                }
                if (s.GetRight() > maxCoord)
                {
                    s.SetRight(maxCoord);
                }
            }
            else
            {
                if (s.GetTop() < 0)
                {
                    s.SetTop(0);
                }
                if (s.GetBottom() > maxCoord)
                {
                    s.SetBottom(maxCoord);
                }
            }

        }

        /// <summary>
        /// Rescales the figure horizontally.
        /// </summary>
        /// <param name="s">Figure to scale.</param>
        /// <param name="original">Original figure before any rescaling.</param>
        /// <param name="anchor">Indicates which side will be moved. Opposite will
        /// stay as is.</param>
        /// <param name="dx">Amount to resize.</param>
        /// <param name="canvas">Canvas to keep resized shape within.</param>
        /// <param name="multiSelectBounds">If there's more than one figure selected,
        /// a <see cref="Rectangle"/> that bounds all of them. Null otherwise.</param>
        public static void ResizeX(this Shape s, Shape original, HorizontalAnchor anchor,
            double dx, double canvasWidth, Shape multiSelectBounds = null)
            => Resize(s, original, anchor, dx, canvasWidth, multiSelectBounds);

        /// <summary>
        /// Rescales the figure horizontally.
        /// </summary>
        /// <param name="s">Figure to scale.</param>
        /// <param name="original">Original figure before any rescaling.</param>
        /// <param name="anchor">Indicates which side will be moved. Opposite will
        /// stay as is.</param>
        /// <param name="dy">Amount to resize.</param>
        /// <param name="canvas">Canvas to keep resized shape within.</param>
        /// <param name="multiSelectBounds">If there's more than one figure selected,
        /// a <see cref="Rectangle"/> that bounds all of them. Null otherwise.</param>
        public static void ResizeY(this Shape s, Shape original, VerticalAnchor anchor,
            double dy, double canvasHeight, Shape multiSelectBounds = null)
            => Resize(s, original, anchor, dy, canvasHeight, multiSelectBounds);
    }
}
