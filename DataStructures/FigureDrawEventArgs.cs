﻿using System;
using System.Windows.Shapes;

namespace VectorAneurysm.DataStructures
{
    /// <summary>
    /// Class that represents event args for ShapeDrawer class.
    /// The only property is associated Shape instance that is drawn by this class.
    /// </summary>
    public class FigureDrawEventArgs : EventArgs
    {
        /// <summary>
        /// Property to store shape information in.
        /// </summary>
        public Shape AffectedShape { get; private set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="shape">Shape associated with this instance.</param>
        public FigureDrawEventArgs(Shape shape)
        {
            AffectedShape = shape;
        }
    }
}
