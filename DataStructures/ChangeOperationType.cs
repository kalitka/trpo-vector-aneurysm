﻿namespace VectorAneurysm.DataStructures
{
    /// <summary>
    /// Represents various operations that do not add and/or remove figures
    /// from the canvas.
    /// </summary>
    public enum ChangeOperationType
    {
        /// <summary>
        /// Figure's stroke style was changed.
        /// </summary>
        DashStyleChanged,
        /// <summary>
        /// Figure's fill color was changed.
        /// </summary>
        FillChanged,
        /// <summary>
        /// Figure was moved.
        /// </summary>
        Moved,
        /// <summary>
        /// A point of polyline or polygon was dragged.
        /// </summary>
        PointMoved,
        /// <summary>
        /// Figure was both resized and maybe moved.
        /// </summary>
        ResizedMoved,
        /// <summary>
        /// Figure's stroke color was changed.
        /// </summary>
        StrokeChanged,
        /// <summary>
        /// Figure's StrokeThickness was changed.
        /// </summary>
        ThicknessChanged
    }
}
