﻿using System.Windows.Media;

namespace VectorAneurysm.DataStructures
{
    /// <summary>
    /// Class that represents available stroke type to use in both UI and code.
    /// </summary>
    public class StrokeTypeItem
    {
        /// <summary>
        /// Human-readable name to display in the UI.
        /// </summary>
        public string Caption { get; private set; }

        /// <summary>
        /// Actual pattern representation to use in code as StrokeDashArray in Shapes
        /// </summary>
        public DoubleCollection Pattern { get; private set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="caption">Caption to display.</param>
        /// <param name="patternParts">Pattern parts: first and next evens are for filled
        /// parts length, odds for empty parts. If zero arguments given, solid stroke
        /// is assumed.</param>
        public StrokeTypeItem(string caption, params double[] patternParts)
        {
            Caption = caption;
            Pattern = patternParts.Length > 0 ?
                new DoubleCollection(patternParts) : null;
        }
    }
}
