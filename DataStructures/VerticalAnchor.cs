﻿namespace VectorAneurysm.DataStructures
{
    /// <summary>
    /// Holds info about how the marker affects vertical scaling of a figure.
    /// </summary>
    public enum VerticalAnchor
    {
        /// <summary>
        /// Marker moves bottom side of the figure.
        /// </summary>
        Bottom,
        /// <summary>
        /// Marker does not affect vertical scaling.
        /// </summary>
        None,
        /// <summary>
        /// Marker moves top side of the figure.
        /// </summary>
        Top
    }
}
