﻿namespace VectorAneurysm.DataStructures
{
    /// <summary>
    /// Represents four sides of shapes.
    /// </summary>
    public enum ShapeSide
    {
        /// <summary>
        /// Bottom side. Bigger Y values.
        /// </summary>
        Bottom,
        /// <summary>
        /// Left side. Smaller X values.
        /// </summary>
        Left,
        /// <summary>
        /// Right side. Bigger X values.
        /// </summary>
        Right,
        /// <summary>
        /// Top side. Smaller Y values.
        /// </summary>
        Top
    }
}
