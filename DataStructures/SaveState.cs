﻿using System;
using System.Collections.Generic;
using System.Windows.Markup;
using System.Windows.Shapes;

using VectorAneurysm.Models.Operations;

namespace VectorAneurysm.DataStructures
{

    /// <summary>
    /// Class that represents save state that contains undo/redo data.
    /// </summary>
    [Serializable]
    public class SaveState
    {
        /// <summary>
        /// Backing field for <see cref="XamledShapes"/> property.
        /// </summary>
        private readonly List<string> _xamledShapes = new List<string>();

        /// <summary>
        /// List of all shapes serialized to XAML.
        /// </summary>
        public IReadOnlyList<string> XamledShapes => _xamledShapes.AsReadOnly();

        /// <summary>
        /// Backing field for <see cref="Done"/> property.
        /// </summary>
        private readonly List<OperationBase> _done = new List<OperationBase>();

        /// <summary>
        /// List of operations done to canvas.
        /// </summary>
        public IReadOnlyList<OperationBase> Done => _done.AsReadOnly();

        /// <summary>
        /// Backing field for <see cref="Undone"/> property.
        /// </summary>
        private readonly List<OperationBase> _undone = new List<OperationBase>();

        /// <summary>
        /// List of operations undone, but saved in file.
        /// </summary>
        public IReadOnlyList<OperationBase> Undone => _undone.AsReadOnly();

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="shapes">Enumerable of all shapers ever added to save.</param>
        /// <param name="done">Enumerable of operations done.</param>
        /// <param name="undone">Enumerable of operations that can be redone.</param>
        public SaveState(IEnumerable<Shape> shapes, IEnumerable<OperationBase> done,
            IEnumerable<OperationBase> undone)
        {
            foreach (var s in shapes)
            {
                _xamledShapes.Add(XamlWriter.Save(s));
            }
            foreach (var op in done)
            {
                _done.Add(op);
            }
            // done should be reversed, because enumerating them breaks LIFO of the stack
            _done.Reverse();
            foreach (var op in undone)
            {
                _undone.Add(op);
            }
            // but undone shouldn't since this is exactly the order we want here
        }
    }
}
