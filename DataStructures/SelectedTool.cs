﻿namespace VectorAneurysm.DataStructures
{
    /// <summary>
    /// Represents tools available in program.
    /// </summary>
    public enum SelectedTool
    {
        /// <summary>
        /// Used to edit existing figures.
        /// </summary>
        Selection,
        /// <summary>
        /// Used to draw line segments without fill.
        /// </summary>
        Line,
        /// <summary>
        /// Used to draw filled polygons.
        /// </summary>
        Polygon,
        /// <summary>
        /// Used to draw round stuff.
        /// </summary>
        Ellipse,
        /// <summary>
        /// Used to draw figures with 90 degree angles.
        /// </summary>
        Rectangle
    }
}
