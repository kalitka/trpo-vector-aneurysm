﻿namespace VectorAneurysm.DataStructures
{
    /// <summary>
    /// Holds info about how the marker affects horizontal scaling of a figure.
    /// </summary>
    public enum HorizontalAnchor
    {
        /// <summary>
        /// Marker moves left side of the figure.
        /// </summary>
        Left,
        /// <summary>
        /// Marker does not affect horizontal scaling.
        /// </summary>
        None,
        /// <summary>
        /// Marker moves right side of the figure.
        /// </summary>
        Right
    }
}
