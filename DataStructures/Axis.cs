﻿namespace VectorAneurysm.DataStructures
{
    /// <summary>
    /// Represents one of two 2D axes used in drawing.
    /// </summary>
    public enum Axis
    {
        /// <summary>
        /// X axis.
        /// </summary>
        Horizontal,
        /// <summary>
        /// Y axis.
        /// </summary>
        Vertical
    }
}
